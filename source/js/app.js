/*!
 * Main JS
 * Copyright 2016 Vuebox (http://www.vuebox.id)
 * Author: Ajie Budiarso
 * Build:
 */

// Declare Require
var $ 			= require('jquery');
var foundation  = require('foundation');
var isMobile 	= require('ismobilejs');
var SVGInjector = require('svg-injector');
var jQueryBridget = require('jquery-bridget');
var Isotope = require('isotope-layout');
var Packery = require('packery');
var imagesLoaded = require('imagesloaded');
var Headroom = require('headroom.js');
var Elevator = require('elevator.js');
var parallax = require('./jquery-parallax.js');
require('./functoggle.js');
require('./lettering.js');
require('./textillate.js');
require('./slabtext.js');
require('./waypoints.js');
require('./color.js');
require('./slimscroll.js');
require('./ticker.js');
require('./tipr.js');
require('./segment.js');
require('jquery-circle-progress');
require('jquery-inview');
require('jquery-scrollify');
require('flickity');
jQueryBridget( 'isotope', Isotope, $ );
jQueryBridget( 'packery', Packery, $ );
imagesLoaded.makeJQueryPlugin( $ );

// Initiate Foundation
$(document).foundation();

$(document).ready(function($){

	/* ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
	MENU HEADROOM
	oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo */
	var myElement = document.querySelector("#menu");
	var headroom = new Headroom(myElement, {
		"offset": 0,
		"tolerance": 5,
		"classes": {
			"initial": "glue",
			"pinned": "glue--pinned",
			"unpinned": "glue--unpinned",
			"top" : "glue--top"
		}
    });
    headroom.init();

	var logo = document.querySelector("#logo");
	var headroom_2 = new Headroom(logo, {
		"offset": 0,
		"tolerance": 5,
		"classes": {
			"initial": "glue",
			"pinned": "glue--pinned",
			"unpinned": "glue--unpinned",
			"top" : "glue--top"
		}
    });
    headroom_2.init();

	/* ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
	MENU ANIMATION
	oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo */
	CSSPlugin.defaultTransformPerspective = 600;
	var toggleMenu = $('#menu, .m-menu');
	var logo = $('#logo');
	var logotitle = $('#logotitle');

	var listItems = $('.cadre li');
	var menusection = $('.menu-section');
	var timeline = new TimelineMax({ paused: false, reversed: true });
	var timeline_2 = new TimelineMax({ paused: false, reversed: true });

	timeline.staggerFromTo(
		listItems, 
		0.3, 
		{ autoAlpha: 0, rotationX: -90, transformOrigin: '50% 0%' }, 
		{ autoAlpha: 1, rotationX: 0, ease: Back.easeOut }, 
		0.1, 
		0.1
	);

	timeline_2.staggerFromTo(
		menusection, 
		0.2, 
		{ height: 0, autoAlpha: 0 }, 
		{ height: '100%',  autoAlpha: 1, ease: Power4.easeOut }, 
		0.1, 
		0.3
	);

	toggleMenu.on('click', function() {
		if( !$(this).hasClass('cross') ){
			$(this).addClass('cross');
			$('body').addClass('noscroll');
			timeline_2.reversed() ? timeline_2.play() : timeline_2.reverse();
			setTimeout(function() {
				menusection.addClass('open');
				setTimeout(function() {
					timeline.reversed() ? timeline.play() : timeline.reverse();
				}, 500);
			}, 500);
			$('.m-menu').find('.fa').toggleClass('fa-bars fa-times');
		} else {
			toggleMenu.removeClass('cross');
			timeline.reversed() ? timeline.play() : timeline.reverse();
			setTimeout(function() {
				menusection.removeClass('open');
				$('body').toggleClass('noscroll');
				setTimeout(function() {
					timeline_2.reversed() ? timeline_2.play() : timeline_2.reverse();
				}, 500);
			}, 1000);
			$('.m-menu').find('.fa').toggleClass('fa-times fa-bars');
		}
	});

	if( $('#homepage').length ){
		$('.cadre li a').on('click', function(e){
			var target = $(this).attr('href').split('#')[1];
			if (/#/.test(this.href)) {
				e.preventDefault();
				toggleMenu.removeClass('cross');
				timeline.reversed() ? timeline.play() : timeline.reverse();
				setTimeout(function() {
					menusection.removeClass('open');
					$('body').toggleClass('noscroll');
					setTimeout(function() {
						timeline_2.reversed() ? timeline_2.play() : timeline_2.reverse();
					}, 500);
				}, 1000);
				$.scrollify.disable();

				$('html, body').animate({
			        scrollTop: $("#"+target).offset().top
			    }, 500, function(){
			    	$.scrollify.enable();
			    });
			}
		});
	}

	/* ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
	MAIN SLIDESHOW
	oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo */
	var $mainslide = $('.mainSlideshow').flickity({
		cellAlign: 'center',
		contain: false,
		prevNextButtons: false,
		pageDots: false,
		autoPlay: 5000,
		wrapAround: false,
		pauseAutoPlayOnHover: false,
		draggable: false
	});
	var mainflickity = $mainslide.data('flickity');

	// Parallax
	var parallax2 = $('.mirrorwrapper').parallax();
	var parallax3 = $('.map3d').parallax();

	/*$mainslide.on( 'select.flickity', function() {
		var $callus = $('.hello').fadeIn(100).textillate({
			minDisplayTime: 1000,
			in: {
				shuffle: true,
				delay: 10,
			}
		});
		$callus.textillate('start');
	});*/


	/* ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
	PORTFOLIO EFFECT
	oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo */

	$('.portfolio-item, .case-item').each(function(){
		var link = $(this).find('a'),
			thisitem = $(this),
			target = link.attr('href');
		link.click(function(e){
			if( link.hasClass('noajax') ){
				
			} else {
				e.preventDefault();
				if ( '#' != target ) {
					$.ajax({
						type: "POST",
						url: target,
						success: function(data){
							$.scrollify.disable();
							$('body').prepend($(data));
							$(data).find('.portfolio-canvas').imagesLoaded().always( function( instance ) {
							  	setTimeout(function() {
							  		$('.portfolio-canvas').addClass('is_appear');
							  		$('body').addClass('noscroll');
							  		$('.closer').click(function(){
							  			$('.portfolio-canvas').removeClass('is_appear');
							  			$('body').removeClass('noscroll');
							  			$('.portfolio-canvas').remove();
							  			$.scrollify.enable();
							  		});
							  	}, 200);
							});
						}
				    });
				}
			}
			
		});
	});

	$('.portfolio-item').each(function(){
		$(this).on('inview', function(event, isInView) {
			if (isInView) {
				$(this).addClass('visible');
			} 
		});
	});

	/* ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
	SECTION SLIDE
	oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo */
	if (!isMobile.phone) {
		if( $('#homepage').length ){
			$.scrollify({
		       section : "section",
		       setHeights: false,
		       updateHash: false,
		       scrollbars: false,
		       easign: "easeInOutCubic",
		       scrollSpeed: 500,
		       overflowScroll: true,
		       standardScrollElements: '.slimScrollDiv',
		       before: function(){
		       		$('.topbarheadline').removeClass('appear');
		       }
		   	});

			$('.scroll_div').slimScroll({
		        height: '100vh'
		    });

		   	$('.scroll_div').slimScroll().bind('slimscroll', function(e, pos){
		   	    if( pos == 'bottom' ){
		   	    	setTimeout(function() {
		   	    		$.scrollify.next();
		   	    	}, 300);
		   	    } else if( pos == 'top' ){
		   	    	setTimeout(function() {
		   	    		$.scrollify.previous();
		   	    	}, 300);
		   	    }
		   	});

			$('.portfolio_scroll').slimScroll({
		        height: '100vh'
		    });

		   	$('.portfolio_scroll').slimScroll().bind('slimscroll', function(e, pos){
		   	    if( pos == 'top' ){
		   	    	$.scrollify.previous();
		   	    }
		   	});
		   	
		   	// Text Animation "Contact NOW"
		   	$('#whatwedo').on('inview', function(event, isInView) {
		   		if (isInView) {
		   			setTimeout(function() {
		   				$('.slimScrollDiv').addClass('appear');
		   			}, 500);
		   		} else {
		   			setTimeout(function() {
		   				$('.slimScrollDiv').removeClass('appear');
		   			}, 500);
				}
		   	});
		}
	} 

	/* ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
	GLOBAL
	oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo */
	
	/*
	if( $("#homepage").length ){
		// Intro
		if(! isMobile.any ){
			$('.intro').css({
			  'margin-top': -($('.intro').height() / 2)
			});
		}

		$('.tlt').hide();

		// Selecting the container element
		var el = document.querySelector('.my-text');
		var el_2 = document.querySelector('.my-text-2');
		var el_3 = document.querySelector('.my-text-3');

		// All the possible options (these are the default values)
		// Remember that every option (except individualDelays) can be defined as single value or array
		var options = {
			size : 400,
			weight : 10,
			rounded : false,
			color: ['#3B3E3B','#DC6A28','#8BC34A','#4585B7','#E24444','#DC6A28','#8BC34A','#4585B7'],
			duration: 1,
			fade: 1,
			delay: [0.7,0.6,0.5,0.4,0.3,0.2,0.1,0],
			individualDelays: true,
			easing: d3_ease.easeCubicOut.ease,
			callback: function(){
		        $('.cl-effect-2 a').each(function(t){
		        	var time = parseInt((t+1)*75);
		        	var el = $(this);
		        	setTimeout(function() {
		        		el.addClass('appear');
		        	}, time);
		        });
		    }
		};
		var options_2 = {
			size : 400,
			weight : 10,
			rounded : false,
			color: ['#e22525','#ffba00','#ff288e','#d28c46','#53bdc1','#00ff4c','#E24444','#4585B7'],
			duration: 2,
			fade: 1,
			delay: [0.7,0.6,0.5,0.4,0.3,0.2,0.1,0],
			individualDelays: true,
			easing: d3_ease.easeCubicOut.ease,
			callback: function(){ 
		        //$('#menu, .head_branding').addClass('appear');
		    }
		};
		var options_3 = {
			size : 400,
			weight : 10,
			rounded : false,
			color: '#000',
			duration: 3,
			fade: 1,
			delay: [0.7,0.6,0.5,0.4,0.3,0.2,0.1,0],
			individualDelays: true,
			easing: d3_ease.easeCubicOut.ease
		};

		

		// Initializing the text (Letters parameters: container-element, options)
		var myText = new Letters(el, options);
		var myText_2 = new Letters(el_2, options_2);
		var myText_3 = new Letters(el_3, options_3);

		myText.hide();
		myText_2.hide();
		myText_3.hide();

		myText.show(options);
		myText_2.show(options_2);
		myText_3.show(options_3);
	}
	*/

	// Servicebox Hover
	$('.service_box').hover(function(){
		var thisbox = $(this),
			overlay = thisbox.attr('data-overlay');
		$('.service_box').removeClass('active');
		thisbox.addClass('active');
		$('.overlay-'+overlay).css("z-index", '3').siblings('.bigbg').css("z-index", '0');
	});
	
	// first section
	$("#menu").addClass('appear');
	$(".head_branding").addClass('appear');
	if( $('#homepage').length ){
		$('#started').on('inview', function(event, isInView) {
			if (isInView) {
				$("#menu").removeClass('appear');
				$(".head_branding").removeClass('appear');
			} else { 
				$("#menu").addClass('appear');
				$(".head_branding").addClass('appear');
			}
		});
	} else {
		$(window).on('load', function(){
			$('#menu, .head_branding').addClass('appear');
		});
	}
	
	// JDI SERVICES
	$('#jdiservices').on('inview', function(event, isInView) {
		if (isInView) {
			$('#jdiservices').addClass('appear');
			$('.section-titler span').not('.jdiservices').removeClass('appear');
			if( isMobile.any ){
				setTimeout(function() {
					$('.jdiservices').addClass('appear');
				}, 1600);
			} else {
				$('.jdiservices').addClass('appear');
			}
		} else {
			$('#jdiservices').removeClass('appear');
			$('.jdiservices').removeClass('appear');
			//$('.jdiservices').removeClass('appear');
		}
	});

	$('#started').on('inview', function(event, isInView) {
		if (isInView) {
			$('.icon-scroll').addClass('darkmode');
		} else {
			$('.section-titler span.icon-scroll').removeClass('darkmode');
		}
	});

	$('#startup').on('inview', function(event, isInView) {
		if (isInView) {
			$('.ourstartup').addClass('appear');
			$('.section-titler span').not('.ourstartup').removeClass('appear');
		} 
	});

	$('#smspayment').on('inview', function(event, isInView) {
		if (isInView) {
			$('.smspayment').addClass('appear');
			$('.section-titler span').not('.smspayment').removeClass('appear');
		} 
	});
	
	$('#career').on('inview', function(event, isInView) {
		if (isInView) {
			$('.career').addClass('appear');
		} 
	});

	$('#aboutjdi').on('inview', function(event, isInView) {
		if (isInView) {
			$('.topbarheadline.aboutjdi').addClass('appear');
			$('.section-titler span').not('.aboutjdi').removeClass('appear');
			$('.section-titler span.aboutjdi').addClass('appear');
		} 
	});

	$('#clients').on('inview', function(event, isInView) {
		if (isInView) {
			$('.clients').addClass('appear');
			$('.section-titler span').not('.clients').removeClass('appear');
			$('.icon-scroll').addClass('darkmode');
		} else {
			$('.icon-scroll').removeClass('darkmode');
		}
	});

	$('#works').on('inview', function(event, isInView) {
		if (isInView) {
			$('.topbarheadline.works').addClass('appear');
			$('.section-titler span').not('.works').removeClass('appear');
			$('.section-titler span.works').addClass('appear');
			setTimeout(function() {
				$('.icon-scroll').addClass('darkmode');
			}, 500);
		} else {
			$('.icon-scroll').removeClass('darkmode');
		}
	});

	$('.su_item').each(function(){
		var kampang = $(this),
			gambar = kampang.find('.mockup'),
			clicked = false;

		gambar.on( 'click', function() {
			var kampangbaru = $(this).parent('.su_item');

			$('.su_item').not(kampangbaru).removeClass('opened');
			kampangbaru.toggleClass('closed opened');
			
		});
	});

	$('#startup').on('inview', function(event, isInView) {
		if (isInView) {
			$('.su_item').each(function(t){
				var thetime = t * 100;
				var el = $(this);
				setTimeout(function() {
					el.addClass('appear');
				}, thetime);
			});
			$('#startup').addClass('appear');
		} else {
			$('#startup').removeClass('appear');
		}
	});

	window.onload = function() {
	  var elevator = new Elevator({
	    element: document.querySelector('.backtotop'),
	    mainAudio: './assets/audio/elevator.mp3',
	    endAudio: './assets/audio/ding.mp3',
	    startCallback: function() {
			$.scrollify.disable();
			$('body').addClass('is_scrolling');
		},
		endCallback: function() {
			$.scrollify.enable();
			$('body').removeClass('is_scrolling');
			$('.topbarheadline').removeClass('appear');
		}
	  });
	}

	// show back to top button when it reaches the   
	$('#totoptrigger').on('inview', function(event, isInView){
		if( isInView ){
			$('.backtotop').addClass('appear');
		} else {
			$('.backtotop').removeClass('appear')
		}
	});



	/* ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
	CLIENT's LOGO, only for mobile
	oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo */
	if( isMobile.any ){
		$('.m_flickity').each(function(){
			$(this).flickity({
				// options
				cellAlign: 'left',
				contain: false,
				prevNextButtons: false,
				freeScroll: true,
				freeScrollFriction: 0.03,
				pageDots: false,
				autoPlay: true,
				wrapAround: true,
				pauseAutoPlayOnHover: false
			});
		});
	}

	// CLIENT's SLIDESHOW
	function autoslide(){
		if( !$('.clientPages > div.active').next().length ){
			$('.clientPages > div:nth-child(1)').trigger('click');
		} else {
			$('.clientPages > div.active').next().trigger('click');
		}
	}

	var intervalID = setInterval(autoslide, 5000);

	$('.clientPages > div').on('click', function(){
		var disbtn = $(this);
		var indexbtn = $(this).attr('data-index');
		var slide = $('.clientList').attr('data-slide');

		$(this).addClass('active').siblings('.clientPages > div').removeClass('active');

		$('.clientSlideContainer .clientList:nth-child('+indexbtn+')').addClass('active').siblings('.clientList').removeClass('active');

		clearInterval(intervalID);
		intervalID = setInterval(autoslide, 5000);
	});

	// SMS GATEWAY SLIDESHOW
	var buttonclick = false;
	var earlypause = true;
	if( earlypause == true ){
		var slidespeed = false;
	} else {
		var slidespeed = 5000;
	}

	var $carousel = $('.sms_slideshow').flickity({
		// options
		cellAlign: 'left',
		contain: false,
		prevNextButtons: false,
		pageDots: false,
		autoPlay: 5000,
		wrapAround: false,
		pauseAutoPlayOnHover: false,
		draggable: false
	});
	var flkty = $carousel.data('flickity');
	flkty.pausePlayer();

    // Pagination progress
    var circleoptions = {
		value: 1,
		size: 36,
		thickness: 5,
		animation: {duration: 5000},
		emptyFill: 'rgba(0,0,0,.2)',
		fill: {color: 'rgba(255,255,255,.5)'}
	};

	// elements
	var $cellButtonGroup = $('.button-group--cells');
	var $cellButtons = $cellButtonGroup.find('.button');

	// update selected cellButtons
	$carousel.on( 'select.flickity', function() {
	  $cellButtons.filter('.is-selected')
	    .removeClass('is-selected');
	  $cellButtons.eq( flkty.selectedIndex )
	    .addClass('is-selected');
	});

	// select cell on button click
	$cellButtonGroup.on( 'click', '.button', function() {
	  var index = $(this).index();
	  $carousel.flickity( 'select', index );
	  flkty.pausePlayer();
	  buttonclick = true;
	});
	// previous
	$('.button--previous').on( 'click', function() {
	  var index = $(this).index();
	  $carousel.flickity( 'select', index );
	  flkty.pausePlayer();
	  buttonclick = true;
	});
	// next
	$('.button--next').on( 'click', function() {
	  $carousel.flickity('next');
	  buttonclick = true;
	  if( earlypause == true ){
	  	flkty.playPlayer();
	  }
	  flkty.pausePlayer();
	});

	// Slide Pagination Tooltip
	$('.tip').tipr({
          'speed': 300,
          'mode': 'above',
          'space': 70
    });

	// Line Animation SVG
	var path = document.querySelector('path.st0');
	var pathlength = path.getTotalLength(); // 988.0062255859375
	var neg_pathlength = -(path.getTotalLength()); // 988.0062255859375
	path.style.transition = path.style.WebkitTransition =
	  'none';
	path.style.strokeDasharray = pathlength + ' ' + pathlength;
	path.style.strokeDashoffset = pathlength;
	path.getBoundingClientRect();
	path.style.transition = path.style.WebkitTransition =
	  'stroke-dashoffset 1.5s ease-in-out';

	// There are 4 slides, devide the length into 4
	var eachLength = parseInt( pathlength / 4 );
	var is_paused = false;

	$('.sms_slideshow').on('inview', function(event, isInView) {
		if (isInView) {
			if( ! isMobile.any ){
				var theindex = parseInt( flkty.selectedIndex + 1 );
				var supposedvalue = parseInt( eachLength * theindex );
				var offset = pathlength - supposedvalue;
				path.style.strokeDashoffset = offset;
			}
		} else {
			if( ! isMobile.any ){
				path.style.strokeDashoffset = ''+pathlength+'';
				is_paused =  true;
			}
		}
	});

	$('#seesample').on('click', function(e){
		e.preventDefault();
		if( earlypause == true ){
			$carousel.flickity( 'select', 1 );
			flkty.playPlayer();
			$('#prgrs_2').circleProgress(circleoptions);
		}
	});

	var currentslide = null;

	$carousel.on( 'select.flickity', function() {
		if( ! isMobile.any ){
			var theindex = parseInt( flkty.selectedIndex + 1 );
			var supposedvalue = parseInt( eachLength * theindex );
			var offset = pathlength - supposedvalue;
			path.style.strokeDashoffset = offset;
		}

		// redraw circle progress
		if( theindex != 1  ){
			$('#prgrs_'+theindex).circleProgress(circleoptions);
			setTimeout(function() {
				flkty.unpausePlayer();
			}, 500);
		} else {
			flkty.pausePlayer();
			$('#prgrs_4').circleProgress(circleoptions).on('circle-animation-end', function(event, progress, stepValue) {
				is_paused = true;
			});
		}

		currentslide = theindex;
	});

	if( ! isMobile.any ){
		$("#example_2").newsTicker({
			base : {
				time: 40000,
			}
		});
	}
});

jQuery(window).on('load', function(){

	if( $('.slab').length ){
		var $height = $(window).height();

		if( $height <= 700 ){
			var $maxFontSize = 200;
		} else {
			var $maxFontSize = 200; 
		}
		/*$('.slab').slabText({
			viewportBreakpoint: 240,
			wrapAmpersand: true,
			minWordsPerLine: 1,
			maxFontSize: $maxFontSize,
			minCharsPerLine: 1,
			fontRatio: '0.1'
		});*/
	}

	if( $('.slab_2').length && !isMobile.any ){
		$('.slab_2').slabText({
			viewportBreakpoint: 500,
			minWordsPerLine: 5,
			minCharsPerLine:10,
			wrapAmpersand: true
		});
	}

	/* ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
	BODY BACKGROUND
	oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo */
	// HSL Colors
	/*var colors = [
	  [29, 117, 195],
	  [97, 20, 204],
	  [59, 151, 160],
	  [154, 181, 88]
	]*/

	/*if( $("#homepage").length ){
		var colors = $("#home").data("colors");
		var colors_array = colors.split(":");
		var numcolors = colors_array.length;

		var RGBs = [];
		for(var i = 0; i < numcolors; i++) {
			RGBs[i] = [];
			var c = colors_array[i].split(",");
			
			RGBs[i][0] = c[0];
			RGBs[i][1] = c[1];
			RGBs[i][2] = c[2];
		}

		var dRGBs = [];
		for(var i = 0; i < (numcolors - 1); i++) {
			dRGBs[i] = [];
			
			dRGBs[i][0] = RGBs[i][0] - RGBs[i+1][0];
			dRGBs[i][1] = RGBs[i][1] - RGBs[i+1][1];
			dRGBs[i][2] = RGBs[i][2] - RGBs[i+1][2];
		}

		$(window).scroll(function() {
			var position = $(this).scrollTop();
			var view = $(this).height();
			var height = $('.batas_bg').height();

			if( position < height ){
				var travel = height - view;
				var percent = position / travel;
				var level = Math.floor(percent * (numcolors - 1));
				var plevel = percent * (numcolors - 1);
				
				var dlevel = Math.floor(level);
				if(Math.floor(level) == (numcolors - 1)) {
					dlevel = Math.floor(level) - 1;
				}
				
				if(plevel > 1) {
					plevel = plevel - dlevel;
				}
				
				var nRed = (RGBs[dlevel][0] - Math.round(dRGBs[dlevel][0] * plevel));
				var nGreen = (RGBs[dlevel][1] - Math.round(dRGBs[dlevel][1] * plevel));
				var nBlue = (RGBs[dlevel][2] - Math.round(dRGBs[dlevel][2] * plevel));
				
				$("body").css("background-color", "rgb(" + nRed + "," + nGreen + "," + nBlue + ")");
			}
			
		});
	}*/
	
	// Make the Thumbnail's Height Equal with the Headline
	var windowsize = $(window).width();
	if( windowsize > 768 ){
		$('.portfolio-item').each(function(){
			var projectdescHeight = $(this).find('.project-description').outerHeight();
			$(this).find('.project-thumb').height(projectdescHeight);
		});
	}

	if( $('#contactpage').length ){
		$('.footer-reveal').parent('.relative').hide(function(){$(this).remove();});

		$('#contactpage').addClass('animateplease');

	}
});