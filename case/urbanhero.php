<div class="portfolio-canvas">
	<div class="portfolio-inner">
		<!-- Closer -->
		<div class="closer"></div>

		<!-- Thumbnail -->
		<figure class="main-image">
			<img src="assets/img/portfolio/simple/urbanhero.png" alt="">
			<h1>GARNIER MEN: URBAN HERO</h1>
		</figure>

		<div class="portfolio-content">
			<div class="row">
				<div class="large-9 large-centered column">
					<div class="row">
						<div class="large-8 column">
							<div class="description">
									<p>URBAN HUNT is the 2nd national series campaign of Garnier Men Urban Hero. This campaign aimed to invite all the youngster and people with young spirit to tell the story about their contribution to the community (which divided into 5 category) and the impact the society. The best story is they who had most like for each category.</p>
							</div> <!-- .description -->
						</div> <!-- .large-8 column -->
						<div class="large-4 column">
							<ul class="portfolio-meta-info">
								<li>
									<h4>PLATFORM</h4>
									<p>
										This system provided with CMS for data monitoring and reporting
									</p>
								</li>
								<li>
									<h4>YEAR</h4>
									<p>
										2015
									</p>
								</li>
								<li>
									<h4>DIGITAL ADVERTISING</h4>
									<p>
										URBAN HUNT is the 2nd national series campaign of Garnier Men Urban Hero. This campaign aimed to invite all the youngster and people with young spirit to tell the story about their contribution to the community (which divided into 5 category) and the impact the society. The best story is they who had most like for each category.
									</p>
								</li>
								<li>
									<h4>RESULT</h4>
									<p>
										Total Website Traffic : <strong>183,646</strong> (21 March 2016)<br>
										Total Registration : <strong>11,450</strong><br>
										Total Submission : <strong>980</strong><br>
									</p>
								</li>
							</ul> <!-- portfolio-meta-info -->
						</div> <!-- large-4 column -->
					</div> <!-- .row -->
				</div> <!-- .large-9 large-centered column -->
			</div> <!-- .row -->
			<div class="showcase__images urbanhero">
				<img src="assets/img/portfolio/single/urban_hunt%20(1).jpg" alt="">
				<img src="assets/img/portfolio/single/urban_hunt%20(2).jpg" alt="">
				<img src="assets/img/portfolio/single/urban_hunt%20(3).jpg" alt="">
				<img src="assets/img/portfolio/single/urban_hunt%20(4).jpg" alt="">
				<img src="assets/img/portfolio/single/urban_hunt%20(5).jpg" alt="">
				<img src="assets/img/portfolio/single/urban_hunt%20(6).jpg" alt="">
				<img src="assets/img/portfolio/single/urban_hunt%20(7).jpg" alt="">
				<img src="assets/img/portfolio/single/urban_hunt%20(8).jpg" alt="">
				<img src="assets/img/portfolio/single/urban_hunt%20(9).jpg" alt="">
				<img src="assets/img/portfolio/single/urban_hunt%20(10).jpg" alt="">
				<img src="assets/img/portfolio/single/urban_hunt%20(11).jpg" alt="">
				<img src="assets/img/portfolio/single/urban_hunt%20(12).jpg" alt="">
		 	</div>
		</div> <!-- .portfolio-content -->
		
		
	</div>
</div>