<div class="portfolio-canvas">
	<div class="portfolio-inner">
		<!-- Closer -->
		<div class="closer"></div>

		<!-- Thumbnail -->
		<figure class="main-image">
			<img src="assets/img/portfolio/simple/matahari.png" alt="">
			<h1>MATAHARI MALL
				<span class="portfolio-subheadline">MMDC NATIONAL ROADSHOW "WHEEL OF FORTUNE"</span>
			</h1>
		</figure>

		<div class="portfolio-content">
			<div class="row">
				<div class="large-9 large-centered column">
					<div class="row">
						<div class="large-8 column">
							<div class="description">
									<p>In order MMDC E-Commerce launching, MMDC team do national roadshow and create direct contact (BTL) and educate consumer at the same time. The goal of this campaign to achieving number of registration as much as possible, and reward them by participating on Wheel of Fortune to get interesting goodie bag.</p>
							</div> <!-- .description -->
						</div> <!-- .large-8 column -->
						<div class="large-4 column">
							<ul class="portfolio-meta-info">
								<li>
									<h4>PLATFORM</h4>
									<p>
										This system provided with CMS for data monitoring and reporting
									</p>
								</li>
								<li>
									<h4>YEAR</h4>
									<p>
										2015
									</p>
								</li>
								<li>
									<h4>DIGITAL ADVERTISING</h4>
									<p>
										Facebook and Twitter Campaign Promo, BTL (Booth at all Lippo Group commercial brand such as Giant, Cinemaxx, UPH etc)
									</p>
								</li>
								<li>
									<h4>RESULT</h4>
									<p>
										Total Participants : <strong>2,778</strong><br>
										Total Registration with Facebook : <strong>2,516</strong><br>
										Total Registration with Twitter : <strong>262</strong><br>
									</p>
								</li>
							</ul> <!-- portfolio-meta-info -->
						</div> <!-- large-4 column -->
					</div> <!-- .row -->
				</div> <!-- .large-9 large-centered column -->
			</div> <!-- .row -->
			<div class="showcase__images mataharimall">
				<img src="assets/img/portfolio/single/matahari-00.jpg" alt="">
				<img src="assets/img/portfolio/single/matahari-01.jpg" alt="">
				<img src="assets/img/portfolio/single/matahari-02.jpg" alt="">
				<img src="assets/img/portfolio/single/matahari-03.jpg" alt="">
				<img src="assets/img/portfolio/single/matahari-04.jpg" alt="">
				<img src="assets/img/portfolio/single/matahari-05.jpg" alt="">
				<img src="assets/img/portfolio/single/matahari-06.jpg" alt="">
				<img src="assets/img/portfolio/single/matahari-07.jpg" alt="">
				<img src="assets/img/portfolio/single/matahari-08.jpg" alt="">
				<img src="assets/img/portfolio/single/matahari-09.jpg" alt="">
		 	</div>
		</div> <!-- .portfolio-content -->
		
		
	</div>
</div>