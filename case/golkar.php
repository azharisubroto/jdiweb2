<div class="portfolio-canvas">
	<div class="portfolio-inner">
		<!-- Closer -->
		<div class="closer"></div>

		<!-- Thumbnail -->
		<figure class="main-image">
			<img src="assets/img/portfolio/single/golkar-header.jpg" alt="">
			<h1>PARTAI GOLKAR
				<span class="portfolio-subheadline">A Political Party</span>
			</h1>
		</figure>

		<div class="portfolio-content">
			<div class="row">
				<div class="large-9 large-centered column">
					<div class="row">
						<div class="large-8 column">
							<div class="description">
									<p>The Party of the Functional Groups (Indonesian: Partai Golongan Karya) is a political party in Indonesia. Originally known as Sekber Golkar (Sekretariat Bersama Golongan Karya, or Joint Secretariat of Functional Groups).</p>
									<p>Golkar was the ruling party from 1973 to 1999, under Suharto's New Order regime (1966-98) and the brief presidency of B. J. Habibie (1998-99). It had been a part of the ruling coalition of President Susilo Bambang Yudhoyono's ruling coalition since 2004. In 2014 Yudhoyono was succeeded by President Joko Widodo.</p>
							</div> <!-- .description -->
						</div> <!-- .large-8 column -->
						<div class="large-4 column">
							<ul class="portfolio-meta-info">
								<li>
									<h4>PLATFORM</h4>
									<p>
										LARAVEL
									</p>
								</li>
								<li>
									<h4>YEAR</h4>
									<p>
										2016
									</p>
								</li>
								<li>
									<h4>TECHNIQUES</h4>
									<p>
										HTML5/SASS/JS, PHP, NODEJS
									</p>
								</li>
							</ul> <!-- portfolio-meta-info -->
						</div> <!-- large-4 column -->
					</div> <!-- .row -->
				</div> <!-- .large-9 large-centered column -->
			</div> <!-- .row -->
			<div class="showcase__images">
				<img src="assets/img/portfolio/single/golkar.jpg" alt="">
		 	</div>
		</div> <!-- .portfolio-content -->
		
		
	</div>
</div>