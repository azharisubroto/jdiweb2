<div class="portfolio-canvas">
	<div class="portfolio-inner">
		<!-- Closer -->
		<div class="closer"></div>

		<!-- Thumbnail -->
		<figure class="main-image">
			<img src="assets/img/portfolio/simple/1482919217.3295.png" alt="">
			<h1>PLAYWORLD.CO.ID
				<span class="portfolio-subheadline">ENTERTAINMENT AND MEDIA PORTAL</span>
			</h1>
		</figure>

		<div class="portfolio-content">
			<div class="row">
				<div class="large-9 large-centered column">
					<div class="row">
						<div class="large-8 column">
							<div class="description">
									<p>Playworld is JDI’s startup in Entertainment Media Portal</p> 

									<p>Utilizing SMS Payment Gateway through all telecommunication operator (97788 & 97789) for any business purpose like selling and promotion</p> 

									<p>Media Portal with Citizen Journalism concept where all the information came from all citizen around Indonesia and people get earnings from  published content</p>
							</div> <!-- .description -->
						</div> <!-- .large-8 column -->
						<div class="large-4 column">
							<ul class="portfolio-meta-info">
								<li>
									<h4>PLATFORM</h4>
									<p>
										LARAVEL
									</p>
								</li>
								<li>
									<h4>YEAR</h4>
									<p>
										2016
									</p>
								</li>
								<li>
									<h4>TECHNIQUES</h4>
									<p>
										HTML5/SASS/JS, PHP, NODEJS
									</p>
								</li>
							</ul> <!-- portfolio-meta-info -->
						</div> <!-- large-4 column -->
					</div> <!-- .row -->
				</div> <!-- .large-9 large-centered column -->
			</div> <!-- .row -->
			<div class="showcase__images">
				<img src="assets/img/portfolio/single/playworld.jpg" alt="">
				<img src="assets/img/portfolio/single/playworld_2.jpg" alt="">
		 	</div>
		</div> <!-- .portfolio-content -->
		
		
	</div>
</div>