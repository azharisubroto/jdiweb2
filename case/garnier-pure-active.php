<div class="portfolio-canvas">
	<div class="portfolio-inner">
		<!-- Closer -->
		<div class="closer"></div>

		<!-- Thumbnail -->
		<figure class="main-image">
			<img src="assets/img/portfolio/simple/maldives.jpg" alt="">
			<h1>GARNIER PURE ACTIVE</h1>
		</figure>

		<div class="portfolio-content">
			<div class="row">
				<div class="large-9 large-centered column">
					<div class="row">
						<div class="large-8 column">
							<div class="description">
									<p>In order to launch new product Garnier Pure Active, Garnier create a campaign to educate consumer to understand about acne and other skin problem and how to prevent and cure it. This campaign deliver in a microsite under garnier.co.id to raise consumer awareness about this problem which in the same time to promote new Garnier product</p>
							</div> <!-- .description -->
						</div> <!-- .large-8 column -->
						<div class="large-4 column">
							<ul class="portfolio-meta-info">
								<li>
									<h4>PLATFORM</h4>
									<p>
										This system provided with CMS for data monitoring and reporting
									</p>
								</li>
								<li>
									<h4>YEAR</h4>
									<p>
										2015
									</p>
								</li>
							</ul> <!-- portfolio-meta-info -->
						</div> <!-- large-4 column -->
					</div> <!-- .row -->
				</div> <!-- .large-9 large-centered column -->
			</div> <!-- .row -->
			<div class="showcase__images urbanhero" style="background-color: #1273b2">
				<img src="assets/img/portfolio/single/pure-active%20(1).jpg" alt="">
				<img src="assets/img/portfolio/single/pure-active%20(2).jpg" alt="">
				<img src="assets/img/portfolio/single/pure-active%20(3).jpg" alt="">
				<img src="assets/img/portfolio/single/pure-active%20(4).jpg" alt="">
				<img src="assets/img/portfolio/single/pure-active%20(5).jpg" alt="">
		 	</div>
		</div> <!-- .portfolio-content -->
		
		
	</div>
</div>