<div class="portfolio-canvas">
	<div class="portfolio-inner">
		<!-- Closer -->
		<div class="closer"></div>

		<!-- Thumbnail -->
		<figure class="main-image">
			<img src="assets/img/portfolio/simple/sq-valentine.jpg" alt="">
			<h1>SILVERQUEEN CHUNKY LOVE PIC</h1>
		</figure>

		<div class="portfolio-content">
			<div class="row">
				<div class="large-9 large-centered column">
					<div class="row">
						<div class="large-8 column">
							<div class="description">
									<p>In order to celebrate Valentine’s Day, SilverQueen made a campaign “Chunky Love Pic” to invite customer to take a picture with their loved ones using SilverQueen ChunkyBar frame that existed inside the package</p>
							</div> <!-- .description -->
						</div> <!-- .large-8 column -->
						<div class="large-4 column">
							<ul class="portfolio-meta-info">
								<li>
									<h4>PLATFORM</h4>
									<p>
										This system provided with CMS for data monitoring and reporting
									</p>
								</li>
								<li>
									<h4>YEAR</h4>
									<p>
										2015
									</p>
								</li>
								<li>
									<h4>DIGITAL ADVERTISING</h4>
									<p>
										Campaign promo through Facebook
									</p>
								</li>
								<li>
									<h4>RESULT</h4>
									<p>
										Total Registration : <strong>12,015 of Facebook Connect</strong><br>
										Total Submission :  <strong>1,810 of Photo</strong><br>
										Total Engagement : <strong>55,317 Likes</strong><br>
									</p>
								</li>
							</ul> <!-- portfolio-meta-info -->
						</div> <!-- large-4 column -->
					</div> <!-- .row -->
				</div> <!-- .large-9 large-centered column -->
			</div> <!-- .row -->
			<div class="showcase__images">
				<img src="assets/img/portfolio/single/valentine.jpg" alt="">
		 	</div>
		</div> <!-- .portfolio-content -->
		
		
	</div>
</div>