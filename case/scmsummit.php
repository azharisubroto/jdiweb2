<div class="portfolio-canvas">
	<div class="portfolio-inner">
		<!-- Closer -->
		<div class="closer"></div>

		<!-- Thumbnail -->
		<figure class="main-image">
			<img src="assets/img/portfolio/single/scmsummit.jpg" alt="">
			<h1>SCM Summit 2015
				<span class="portfolio-subheadline">Oil &amp; Gas National Conference</span>
			</h1>
		</figure>

		<div class="portfolio-content">
			<div class="row">
				<div class="large-9 large-centered column">
					<div class="row">
						<div class="large-8 column">
							<div class="description">
									<p>SCM Summit is annual national scale event held by SKK Migas (Satuan Kerja Khusus Minyak dan Gas). On year 2015 web platform is applied to simplify registration process. This application also intended for re-registration at the event also tools for participants to access speakers presentation and photo gallery.</p>
							</div> <!-- .description -->
						</div> <!-- .large-8 column -->
						<div class="large-4 column">
							<ul class="portfolio-meta-info">
								<li>
									<h4>PLATFORM</h4>
									<p>
										This system provided with CMS for data monitoring and reporting
									</p>
								</li>
								<li>
									<h4>YEAR</h4>
									<p>
										2015
									</p>
								</li>
								<li>
									<h4>DIGITAL ADVERTISING</h4>
									<p>
										Kompas Paid Advertising, Email and SMS Broadcast
									</p>
								</li>
								<li>
									<h4>RESULT</h4>
									<p>
										Total Website Traffic : <strong>16,667 (21 March 2016)</strong><br>
										Total Registration : <strong>1,201</strong>
									</p>
								</li>
							</ul> <!-- portfolio-meta-info -->
						</div> <!-- large-4 column -->
					</div> <!-- .row -->
				</div> <!-- .large-9 large-centered column -->
			</div> <!-- .row -->
			<div class="showcase__images urbanhero" style="background-color:#87b2c3">
				<img src="assets/img/portfolio/single/scmsummit-big.jpg" alt="">
		 	</div>
		</div> <!-- .portfolio-content -->
		
		
	</div>
</div>