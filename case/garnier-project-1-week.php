<div class="portfolio-canvas">
	<div class="portfolio-inner">
		<!-- Closer -->
		<div class="closer"></div>

		<!-- Thumbnail -->
		<figure class="main-image">
			<img src="assets/img/portfolio/garnier.jpg" alt="">
			<h1>GARNIER ACADEMY PROJECT 1 WEEK
				<span class="portfolio-subheadline">Brand Ambasador</span>
			</h1>
		</figure>

		<div class="portfolio-content">
			<div class="row">
				<div class="large-9 large-centered column">
					<div class="row">
						<div class="large-8 column">
							<div class="description">
									<p>This event calling all women to join “Project 1 Week” where they can get a short training , direction and guidance from celebrity mentor such as Becky Tumewu and Erwin Parengkuan. Before it happen this all participants should register themselves through website and tell their acheivement and upload their photos for the judging process.</p>
							</div> <!-- .description -->
						</div> <!-- .large-8 column -->
						<div class="large-4 column">
							<ul class="portfolio-meta-info">
								<li>
									<h4>PLATFORM</h4>
									<p>
										This system provided with CMS for data monitoring and reporting
									</p>
								</li>
								<li>
									<h4>YEAR</h4>
									<p>
										2015
									</p>
								</li>
								<li>
									<h4>DIGITAL ADVERTISING</h4>
									<p>
										Facebook and Twitter Campaign Promo, and BTL (Campus to Campus Roadshow)
									</p>
								</li>
								<li>
									<h4>RESULT</h4>
									<p>
										Total Website Traffic : <strong>98,873 (21 March 2016)</strong><br>
										Total Manual Registration : <strong>1,409</strong><br>
										Total Facebook Registration : <strong>667</strong><br>
										Total Achievement Submission : <strong>652</strong><br>
									</p>
								</li>
							</ul> <!-- portfolio-meta-info -->
						</div> <!-- large-4 column -->
					</div> <!-- .row -->
				</div> <!-- .large-9 large-centered column -->
			</div> <!-- .row -->
			<div class="showcase__images">
				<img src="assets/img/portfolio/single/project1week.jpg" alt="">
		 	</div>
		</div> <!-- .portfolio-content -->
		
		
	</div>
</div>