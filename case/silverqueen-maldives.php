<div class="portfolio-canvas">
	<div class="portfolio-inner">
		<!-- Closer -->
		<div class="closer"></div>

		<!-- Thumbnail -->
		<figure class="main-image">
			<img src="assets/img/portfolio/simple/maldives.jpg" alt="">
			<h1>Silverqueen Santai di Maldives</h1>
		</figure>

		<div class="portfolio-content">
			<div class="row">
				<div class="large-9 large-centered column">
					<div class="row">
						<div class="large-8 column">
							<div class="description">
									<p>On year 2015, SilverQueen announce the grand prize is Holiday Trip to Maldives for 4 People in 4 days and 3 night. To participate in this national campaign, customer need to buy SilverQueen with special package and put the code inside the packaging to website or SMS</p>
							</div> <!-- .description -->
						</div> <!-- .large-8 column -->
						<div class="large-4 column">
							<ul class="portfolio-meta-info">
								<li>
									<h4>PLATFORM</h4>
									<p>
										This system provided with CMS for data monitoring and reporting
									</p>
								</li>
								<li>
									<h4>YEAR</h4>
									<p>
										2015
									</p>
								</li>
								<li>
									<h4>DIGITAL ADVERTISING</h4>
									<p>
										Campaign promo activity through Facebook
									</p>
								</li>
								<li>
									<h4>RESULT</h4>
									<p>
										Total Website Traffic: <strong>70,594 (21 March 2016)</strong><br>
										Total Registration: <strong> 46,427</strong><br>
										Total Submission: <strong>98,408</strong><br>
										Total Quiz Engagement: <strong>34,801</strong><br>
									</p>
								</li>
							</ul> <!-- portfolio-meta-info -->
						</div> <!-- large-4 column -->
					</div> <!-- .row -->
				</div> <!-- .large-9 large-centered column -->
			</div> <!-- .row -->
			<div class="showcase__images urbanhero" style="background-color: #1273b2">
				<img src="assets/img/portfolio/single/silverqueen-maldives.jpg" alt="">
		 	</div>
		</div> <!-- .portfolio-content -->
		
		
	</div>
</div>