<?php require_once('inc/header.php'); ?>
	
	<!-- ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
	HERO
	oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo -->
	<section class="full_height contact hero" style="background: rgb(37, 154, 255) url(assets/img/map.png) no-repeat center center; background-size: 90%">
		<div class="v-align">
			<div class="row">
				<div class="large-4 small-12 column">
					<h3>THE OFFICE</h3>
					<p>
						Kokan Permata Blok E29<br>
						Jl. Boulevard Bukit Gading Raya <br>
						Kelapa Gading, Jakarta Utara 14240<br>
						DKI Jakarta, Indonesia
					</p>
					<ul class="social">
						<li><a href="https://www.facebook.com/playworldID" target="_BLANK" class="facebook"><i class="fa fa-facebook"></i></a></li>
						<li><a href="https://twitter.com/PlayworldID" target="_BLANK" class="twitter"><i class="fa fa-twitter"></i></a></li>
						<li><a href="https://www.linkedin.com/company/3572988?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A3572988%2Cidx%3A1-1-1%2CtarId%3A1484290768224%2Ctas%3Ajayadata" target="_BLANK" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="https://www.instagram.com/playworldid/" target="_BLANK" class="instagram"><i class="fa fa-instagram"></i></a></li>
					</ul>
				</div>
				<div class="large-4 small-12 column">
					<h3>WORK WITH US</h3>
					<p>Have a project or an idea you’d like to collaborate with us on? Let’s talk.</p>
					<p><a href="mailto:corporate@jayadata.co.id" class="fancy_link">corporate@jayadata.co.id</a></p>
				</div>
				<div class="large-4 small-12 column">
					<h3>Looking for your next job?</h3>
					<p>We’re always on the lookout for the most talented designers, engineers and business people.</p>
					<p><a href="http://www.jobstreet.co.id/en/companies/749164-pt-jayadata-indonesia" class="fancy_link">Check out our jobs listings</a></p>
				</div>
			</div><!-- 
			<div class="row">
				<div class="large-4 large-centered column">
					<br><br><br><br>
					<h3 class="text-center">JOIN OUR NETWORK</h3>
					<ul class="social">
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="#"><i class="fa fa-instagram"></i></a></li>
					</ul>
				</div>
			</div> -->
		</div>
	</section>

<?php require_once('inc/footer.php'); ?>
	