# Nestohuis Boilerplate

Start with `npm install` to install dependencies then run `gulp`.

## Available syntax
* `gulp` - Run default task, build sass and bundle js
* `gulp clean` - Delete sourcemaps
* `gulp img` - Optimize image from source to assets
* `gulp build` - Build production ready

## Dependencies
* [Foundation Site](https://github.com/zurb/foundation-sites) - The most advanced responsive front-end framework in the world. Quickly create prototypes and production code for sites that work on any kind of device.
* [jQuery](https://github.com/jquery/jquery) - jQuery is a fast, small, and feature-rich JavaScript library.
* [SVGInjector](https://github.com/iconic/SVGInjector) - Fast, caching, dynamic inline SVG DOM injection library.
* [WOW](https://github.com/matthieua/WOW/) - Reveal CSS animation as you scroll down a page
* [isMobile](https://github.com/kaimallea/isMobile) - A simple JS library that detects mobile devices.