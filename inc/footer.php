	<!-- ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
	FOOTER
	oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo -->
	<footer id="totoptrigger" class="small_section" style="background: #f0f0f0;border-top: 1px solid #e5e5e5;line-height: 47px;">
		<div class="row">
			<div class="large-12 small-12 column">
				<svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" version="1.1" x="0px" y="0px" viewBox="294.095 226.181 900 600" enable-background="new 294.095 226.181 900 600" xml:space="preserve" class="injected-svg inject site-brand">
					<g>
						<polygon fill="#e1a292" points="835.124,541.36 835.125,541.361 835.126,541.359  "></polygon>
						<path fill="#e1a292" d="M1002.56,273.975c-11.038-7.094-25.852-14.281-38.868-14.31c-30.042-0.066-60.084,0-90.125,0c-22.717,0-51.152,15.125-63.856,33.957L560.264,663.306L491.07,560.794c-12.704-18.832-41.14-33.957-63.857-33.957h-90.125c-1.42,0-2.755-0.117-4.058,0c-19.543,1.731-25.793,16.301-13.882,33.957l133.479,197.762c4.967,7.361,12.438,14.169,20.93,19.755c10.731,7.328,28.95,14.415,40.363,14.415c29.848,0,60.105,0,90.125,0c22.717,0,51.151-15.338,63.856-34.17l248.547-368.123l91.727,135.365v0.4l-91.72,135.771l-81.331-120.609l-86.067,127.283l60.653,89.912c4.964,7.359,12.432,14.058,20.922,19.644c-0.094,0.062-0.183,0.129-0.277,0.19c7.595,4.981,16.176,9.237,24.652,11.719c3.739,1.733,8.466,2.426,14.144,2.426h90.123c22.716,0,51.148-15.116,63.854-33.948l147.913-219.052c0,0,0.446-0.748,0.404-0.801c2.301-3.652,3.637-8.294,3.637-12.932c0-4.672-1.303-8.859-3.637-12.527c-0.365-0.582-0.802-1.074-1.212-1.617l-147.105-218.233C1018.191,286.105,1010.989,279.503,1002.56,273.975z"></path>
						<path fill="#e1a292" d="M541.934,381.937c3.674,6.765,10.793,11.421,19.034,11.421c8.241,0,15.359-4.655,19.034-11.421c18.881-28.201,37.865-56.348,56.867-84.467c8.064-11.955,8.512-22.584,2.854-29.504c-4.271-5.224-11.954-8.328-22.841-8.328h-111.83c-10.809,0-18.55,3.171-22.842,8.328c-5.756,6.917-5.254,17.483,2.856,29.504C504.083,325.579,523.048,353.74,541.934,381.937z"></path>
					</g>
				</svg>
				<div class="legal">
					&copy; JDI - Jayadata Indonesia.
				</div>
			</div>
		</div>
	</footer>
	<div class="clear clearfix"></div>

	<!-- ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
	Empty section
	oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo -->

	<!-- ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
	CONTACT
	oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo -->
	<?php /* 
	<div class="relative" data-stellar-offset-parent="true">
		<div class="footer-reveal" style="background: #e1a292">
			<section class="full_height">
				<div class="v-align">
					<div class="row text-center parallax showme">
						<div class="large-12 column">
							<h2 style="text-transform: uppercase; font-size: 45px;" >
								<a href="#" class="tlt" >Now, Let's Talk About Your Project &rarr;</a>
							</h2>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div> */ ?>
<div class="clear clearfix"></div>
<script src="//cdnjs.cloudflare.com/ajax/libs/gsap/1.16.1/TweenMax.min.js"></script>
<script src="assets/js/app-dist.js"></script>
<?php 
	if( strpos($_SERVER['SCRIPT_NAME'], 'index.php') !== false ){ 
?>
	<script src="assets/js/segment.min.js"></script>
	<script src="assets/js/d3-ease.v0.6.js"></script>
	<script src="assets/js/letters.js"></script>
	<script src="assets/js/tilt.js"></script>
	<script>
		var $ = jQuery;
		$('.case-item').tilt({
		    glare: true,
		    maxGlare: .9,
		    maxTilt: 20,
		    perspective: 1800,
		    scale: 1.05,
		});
	</script>
<?php } ?>

</body>
</html>