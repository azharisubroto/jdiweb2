<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="description" content="Digital Agency that help many brands to achieve their goals in digital strategy, execution and development.">
    <meta name="google-site-verification" content=""/>
    <meta name="keywords" content="jayadata, web design, mobile app, digital campaign" />
    <meta name="copyright" content="PT. Jayadata Indonesia" />
    <meta name="author" content="PT. Jayadata Indonesia" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <meta property="og:title" content="JDI" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://www.jayadata.com" />
    <meta property="og:image" content="http://www.jayadata.com/assets/img/jdi-og.png" />
    <meta property="og:locale" content="id_ID"/>
    <meta property="og:site_name" content="Jayadata Indonesia" />
    <meta property="og:description" content="Digital Agency that help many brands to achieve their goals in digital strategy, execution and development." />

    <meta name="twitter:card" content="summary">
    <meta name="twitter:creator" content="@JayadataIndonesia">
    <meta name="twitter:site" content="@JayadataIndonesia">
    <meta name="twitter:title" content="JDI">
    <meta name="twitter:description" content="Jayadata Indonesia">
    <meta name="twitter:image" content="http://www.jayadata.com/img/jdi-og.jpg" />
	<link rel="shortcut icon" type="image/png" href="assets/img/icons/favicon-194x194.png" sizes="194x194">
	<link href="https://fonts.googleapis.com/css?family=Lato:400,400i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/app-dist.css">
    <title>JDI - Web Agency / Startup / SMS Payment Gateway</title>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-90309182-1', 'auto');
      ga('send', 'pageview');

    </script>

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-52265578-1', 'auto');
      ga('send', 'pageview');

    </script>
    <script src="https://use.typekit.net/mwu5xvs.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>
	
</head>
<body id="home" class="normal" data-colors="37, 154, 255: 255, 72, 72: 255, 72, 72: 144, 90, 255: 136, 183, 72">
    <div class="backtotop show-for-large"><img src="assets/img/up_circle-128.png" alt=""></div>
    <div class="scroll_animation show-for-large">
        <div class='icon-scroll'></div>
    </div>
    
    <!-- ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
    BRANDING
    oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo -->
    <div class="head_branding show-for-large" id="logo">
        <a href="./"><svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" version="1.1" x="0px" y="0px" viewBox="294.095 226.181 900 600" enable-background="new 294.095 226.181 900 600" xml:space="preserve" class="injected-svg inject site-brand">
            <g>
                <polygon fill="#FFFFFF" points="835.124,541.36 835.125,541.361 835.126,541.359  "></polygon>
                <path fill="#FFFFFF" d="M1002.56,273.975c-11.038-7.094-25.852-14.281-38.868-14.31c-30.042-0.066-60.084,0-90.125,0c-22.717,0-51.152,15.125-63.856,33.957L560.264,663.306L491.07,560.794c-12.704-18.832-41.14-33.957-63.857-33.957h-90.125c-1.42,0-2.755-0.117-4.058,0c-19.543,1.731-25.793,16.301-13.882,33.957l133.479,197.762c4.967,7.361,12.438,14.169,20.93,19.755c10.731,7.328,28.95,14.415,40.363,14.415c29.848,0,60.105,0,90.125,0c22.717,0,51.151-15.338,63.856-34.17l248.547-368.123l91.727,135.365v0.4l-91.72,135.771l-81.331-120.609l-86.067,127.283l60.653,89.912c4.964,7.359,12.432,14.058,20.922,19.644c-0.094,0.062-0.183,0.129-0.277,0.19c7.595,4.981,16.176,9.237,24.652,11.719c3.739,1.733,8.466,2.426,14.144,2.426h90.123c22.716,0,51.148-15.116,63.854-33.948l147.913-219.052c0,0,0.446-0.748,0.404-0.801c2.301-3.652,3.637-8.294,3.637-12.932c0-4.672-1.303-8.859-3.637-12.527c-0.365-0.582-0.802-1.074-1.212-1.617l-147.105-218.233C1018.191,286.105,1010.989,279.503,1002.56,273.975z"></path>
                <path fill="#FFFFFF" d="M541.934,381.937c3.674,6.765,10.793,11.421,19.034,11.421c8.241,0,15.359-4.655,19.034-11.421c18.881-28.201,37.865-56.348,56.867-84.467c8.064-11.955,8.512-22.584,2.854-29.504c-4.271-5.224-11.954-8.328-22.841-8.328h-111.83c-10.809,0-18.55,3.171-22.842,8.328c-5.756,6.917-5.254,17.483,2.856,29.504C504.083,325.579,523.048,353.74,541.934,381.937z"></path>
            </g>
        </svg></a>
    </div>
    <!-- <div id="particles"></div> -->

    <!-- ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
    MENU
    oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo -->
	<div id="menu" class="show-for-large">
	   <svg viewBox="0 0 1500 600">
	    <path d="M300,220 C300,220 520,220 540,220 C740,220 640,540 520,420 C440,340 300,200 300,200" id="top"></path>
	    <path d="M300,320 L540,320" id="middle"></path>
	    <path d="M300,210 C300,210 520,210 540,210 C740,210 640,530 520,410 C440,330 300,190 300,190" id="bottom" transform="translate(480, 320) scale(1, -1) translate(-480, -318) "></path>
	   </svg>
       <div class="topbardiv">
           <h2 class="topbarheadline jdiservices" data-title="jdiservices">WHAT WE DO</h2>
           <h2 class="topbarheadline ourstartup">OUR STARTUP</h2>
           <h2 class="topbarheadline smspayment">SMS GATEWAY PROVIDER</h2>
           <h2 class="topbarheadline clients">CLIENTS</h2>
           <h2 class="topbarheadline works">FEATURED CASES</h2>
           <h2 class="topbarheadline career">JOIN WITH US</h2>
           <h2 class="topbarheadline aboutjdi">CONTACT US</h2>
       </div>
	</div>

    <!-- ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
    MOBILE MENU
    oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo -->
    <div class="m-topbar hide-for-large">
        <a href="#" class="m-menu">
            <i class="fa fa-bars"></i>
        </a>
        <a href="./" class="m-logo">
            <img src="assets/img/jdilogo-white.png" alt="">
        </a>
        <div class="section-titler">
            <span class="jdiservices">WHAT WE DO</span>
            <span class="ourstartup">OUR STARTUP</span>
            <span class="smspayment">SMS GATEWAY PROVIDER</span>
            <span class="clients">CLIENTS</span>
            <span class="works">FEATURED CASES</span>
            <span class="career">JOIN WITH US</span>
            <span class="aboutjdi">CONTACT US</span>
        </div>
    </div>
    
    <!-- ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
    MENU CONTENT
    oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo -->
	<div class="menu-section">
        <div class="isolate">
            <div class="diamond_1"></div>
            <div class="diamond_2"></div>
        </div>
        <!-- <div class="diamond_3"></div> -->
        <div class="cadre">
            <nav>
                <ul role="navigation" > 
                    <li><a href="./">HOME</a></li>
                    <li><a href="index.php#jdiservices">WHAT WE DO</a></li>
                    <li><a href="index.php#startup">OUR STARTUP</a></li>
                    <li><a href="index.php#smspayment">SMS GATEWAY PROVIDER</a></li>
                    <li><a href="index.php#clients">CLIENTS</a></li>
                    <li><a href="cases.php">CASES</a></li>
                    <li><a href="about.php">ABOUT</a></li>
                </ul>
            </nav>
        </div>
    </div>