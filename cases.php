<?php require_once('inc/header.php'); ?>
	
	<!-- ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
	HERO
	oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo -->
	<section class="half_height hero">
		<div class="intro text-center">
			<h1 class="slab" data-heading="CASES">CASES</h1>

			<div class="hero_intro">
				<p>
					We thankful to all our beloved clients for having JDI involved in their projects, it's such a great experience for JDI to elevate our skills and also to stabilize our brand name. Here is our some projects in timeline
				</p>
			</div>
		</div>
	</section>

	<!-- ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
	PORTFOLIO
	oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo -->
	<section class="std_section portfolio_section" style="background: #fff;padding: 50px 0 150px;">
		
		<!-- ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
		Portfolio Items
		/**
		 * TO PROGRAMMER:
		 * item ganjil diberi class .odd, yang genap diberi class .even
		 * ODD: Judul project di sebelah kanan, image di sebelah kiri
		 * Even: Judul project di sebelah kiri, image di sebelah kanan
		 */ 
		oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo -->

		<!-- Loop start -->
		<div class="portfolio-item odd">
			<a href="case/playworld.php">
				<div class="row">
					<div class="large-6 large-push-6 small-12 column">
						<div class="project-description">
							<div class="year">
								2016
							</div>
							<h1 class="slab_2 project-title">PLAYWORLD.CO.ID</h1>
							<h1 class="slab_2 project-subtitle">ENTERTAINMENT AND MEDIA PORTAL</h1>
						</div>
					</div>
					<div class="large-6 large-pull-6 small-12 column">
						<div class="project-thumb">
							<img src="assets/img/portfolio/playworld-header.jpg" alt="" >
						</div>
					</div>
				</div>
			</a>
		</div> <!-- end of .portfolio-item -->

		<!-- Loop start -->
		<div class="portfolio-item even">
			<a href="case/golkar.php">
				<div class="row">
					<div class="large-6 small-12 column">
						<div class="project-description">
							<div class="year">
								2016
							</div>
							<h1 class="slab_2 project-title">PARTAI GOLKAR</h1>
							<h1 class="slab_2 project-subtitle">GOLKAR BANGKIT, GOLKAR JAYA, GOLKAR MENANG!</h1>
						</div>
					</div>
					<div class="large-6 small-12 column">
						<div class="project-thumb">
							<img src="assets/img/portfolio/simple/golkar.jpg" alt="" >
						</div>
					</div>
				</div>
			</a>
		</div> <!-- end of .portfolio-item -->

		<!-- Loop start from row -->
		<div class="portfolio-item odd">
			<a href="case/garnier-project-1-week.php">
				<div class="row">
					<div class="large-6 large-push-6 small-12 column">
						<div class="project-description">
							<div class="year">
								2015
							</div>
							<h1 class="slab_2 project-title">BRAND AMBASADOR</h1>
							<h1 class="slab_2 project-subtitle">GARNIER ACADEMY - PROJECT 1 WEEK</h1>
						</div>
					</div>
					<div class="large-6 large-pull-6 small-12 column">
						<div class="project-thumb">
							<img src="assets/img/portfolio/simple/garnier.jpg" alt="" >
						</div>
					</div>
				</div>
			</a>
		</div> <!-- end of .portfolio-item -->

		<!-- Loop start -->
		<div class="portfolio-item even">
			<a href="case/chunkybar.php">
				<div class="row">
					<div class="large-6 small-12 column">
						<div class="project-description">
							<div class="year">
								2016
							</div>
							<h1 class="slab_2 project-title">SILVERQUEEN</h1>
							<h1 class="slab_2 project-subtitle">I WILL NOT SHARE MY CHUNKY BAR</h1>
						</div>
					</div>
					<div class="large-6 small-12 column">
						<div class="project-thumb">
							<img src="assets/img/portfolio/simple/silverqueen.jpg" alt="" >
						</div>
					</div>
				</div>
			</a>
		</div> <!-- end of .portfolio-item -->

		<!-- Loop start -->
		<div class="portfolio-item odd">
			<a href="case/silverqueen-valentine.php">
				<div class="row">
					<div class="large-6 large-push-6 small-12 column">
						<div class="project-description">
							<div class="year">
								2016
							</div>
							<h1 class="slab_2 project-title">VALENTINE CAMPAIGN</h1>
							<h1 class="slab_2 project-subtitle">SILVERQUEEN CHUNKY LOVE PIC</h1>
						</div>
					</div>
					<div class="large-6 large-pull-6 small-12 column">
						<div class="project-thumb">
							<img src="assets/img/portfolio/simple/sq-valentine.jpg" alt="" >
						</div>
					</div>
				</div>
			</a>
		</div> <!-- end of .portfolio-item -->

		<!-- Loop start -->
		<div class="portfolio-item even">
			<a href="case/mataharimall.php">
				<div class="row">
					<div class="large-6 small-12 column">
						<div class="project-description">
							<div class="year">
								2015
							</div>
							<h1 class="slab_2 project-title">MATAHARI MALL</h1>
							<h1 class="slab_2 project-subtitle">MMDC NATIONAL ROADSHOW "WHEEL OF FORTUNE"</h1>
						</div>
					</div>
					<div class="large-6 small-12 column">
						<div class="project-thumb">
							<img src="assets/img/portfolio/simple/matahari.png" alt="" >
						</div>
					</div>
				</div>
			</a>
		</div> <!-- end of .portfolio-item -->

		<!-- Loop start -->
		<div class="portfolio-item odd">
			<a href="case/urbanhero.php">
				<div class="row">
					<div class="large-6 small-12 large-push-6 column">
						<div class="project-description">
							<div class="year">
								2015
							</div>
							<h1 class="slab_2 project-title">GARNIER MEN</h1>
							<h1 class="slab_2 project-subtitle">URBAN HERO</h1>
						</div>
					</div>
					<div class="large-6 small-12 large-pull-6 column">
						<div class="project-thumb">
							<img src="assets/img/portfolio/simple/urbanhero.png" alt="" >
						</div>
					</div>
				</div>
			</a>
		</div> <!-- end of .portfolio-item -->

		<!-- Loop start -->
		<div class="portfolio-item even">
			<a href="case/silverqueen-maldives.php">
				<div class="row">
					<div class="large-6 small-12 column">
						<div class="project-description">
							<div class="year">
								2015
							</div>
							<h1 class="slab_2 project-title">SILVERQUEEN</h1>
							<h1 class="slab_2 project-subtitle">SANTAI DI MALDIVES</h1>
						</div>
					</div>
					<div class="large-6 small-12 column">
						<div class="project-thumb">
							<img src="assets/img/portfolio/simple/maldives.jpg" alt="" >
						</div>
					</div>
				</div>
			</a>
		</div> <!-- end of .portfolio-item -->

		<!-- Loop start -->
		<div class="portfolio-item odd">
			<a href="case/garnier-pure-active.php">
				<div class="row">
					<div class="large-6 small-12 large-push-6 column">
						<div class="project-description">
							<div class="year">
								2015
							</div>
							<h1 class="slab_2 project-title">GARNIER PURE ACTIVE</h1>
							<h1 class="slab_2 project-subtitle">PRODUCT LAUNCHING</h1>
						</div>
					</div>
					<div class="large-6 small-12 large-pull-6 column">
						<div class="project-thumb">
							<img src="assets/img/portfolio/pureactive.png" alt="" >
						</div>
					</div>
				</div>
			</a>
		</div> <!-- end of .portfolio-item -->

		<!-- Loop start -->
		<div class="portfolio-item even">
			<a href="case/scmsummit.php">
				<div class="row">
					<div class="large-6 small-12 column">
						<div class="project-description">
							<div class="year">
								2015
							</div>
							<h1 class="slab_2 project-title">SCM Summit 2015</h1>
							<h1 class="slab_2 project-subtitle">Oil &amp; Gas National Conference</h1>
						</div>
					</div>
					<div class="large-6 small-12 column">
						<div class="project-thumb">
							<img src="assets/img/portfolio/scmsummit.png" alt="" >
						</div>
					</div>
				</div>
			</a>
		</div> <!-- end of .portfolio-item -->
	</section>

	<?php /*
	<!-- ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
	PORTFOLIO DETAIL CANVAS
	oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo -->
	<div class="portfolio-canvas">
		<div class="portfolio-inner">
			<!-- Closer -->
			<div class="closer"></div>

			<!-- Thumbnail -->
			<figure class="main-image">
				<img src="assets/img/portfolio/desk.jpg" alt="">
				<h1>BRAND AMBASADOR</h1>
			</figure>

			<div class="portfolio-content">
				<div class="row">
					<div class="large-9 large-centered column">
						<div class="row">
							<div class="large-8 column">
								<div class="description">
									<p>
										With Calvin being around the studio for some time now, it was finally time to upgrade the Pavlov Visuals website to fit the great visuals they regularly put out. 
									</p>
									<p>
										So content first, responsive, hi-res images, subtle use of colours and animations were the ingredients for this portfolio website - which suck you in to browsing through all their amazing work!
									</p>
								</div> <!-- .description -->
							</div> <!-- .large-8 column -->
							<div class="large-4 column">
								<ul class="portfolio-meta-info">
									<li>
										<h4>PLATFORM</h4>
										<p>
											JDI CMS
										</p>
									</li>
									<li>
										<h4>YEAR</h4>
										<p>
											JDI CMS
										</p>
									</li>
									<li>
										<h4>TECHNIQUES</h4>
										<p>
											HTML5/CSS3/JS, PHP
										</p>
									</li>
								</ul> <!-- portfolio-meta-info -->
							</div> <!-- large-4 column -->
						</div> <!-- .row -->
					</div> <!-- .large-9 large-centered column -->
				</div> <!-- .row -->
				<div class="showcase__images">

		            <img class="showcase__image" sizes="100vw" srcset="http://www.bolden.nl/thumbs/w-green-01-ccbea86f69b7821ae6f64da4faac886d.jpg 400w,
		                http://www.bolden.nl/thumbs/w-green-01-2d9a7f40ac4c0fe4c47772e429f241f1.jpg 600w,
		                http://www.bolden.nl/thumbs/w-green-01-350910af8208f30a356558bcd92bd1ac.jpg 800w,
		                http://www.bolden.nl/thumbs/w-green-01-aa26536b39f3911e2d1ccbf5fba4acb4.jpg 1000w,
		                http://www.bolden.nl/thumbs/w-green-01-2ee1b26a42834649fe23699ed5d366c1.jpg 1400w,
		                http://www.bolden.nl/thumbs/w-green-01-0a6b744348321ae72b90f2e2ff7f0410.jpg 1600w,
		                http://www.bolden.nl/thumbs/w-green-01-bdcf6aec7a6ed0381dd37ee9dfd37455.jpg 1800w" alt="W.GREEN">
		            <img class="showcase__image" sizes="100vw" srcset="http://www.bolden.nl/thumbs/w-green-02-3563660ee9ef2cbd12ca70e871ccd1b2.jpg 400w,
		                http://www.bolden.nl/thumbs/w-green-02-d3b2096668c0a03a4e65357f8bc619f8.jpg 600w,
		                http://www.bolden.nl/thumbs/w-green-02-9cada1d6c2bd5b96e5ed072b9dadb052.jpg 800w,
		                http://www.bolden.nl/thumbs/w-green-02-2e689345703d3e8b70262f67e263a965.jpg 1000w,
		                http://www.bolden.nl/thumbs/w-green-02-3f173dc6a4f56260c7570f677a4fc1e7.jpg 1400w,
		                http://www.bolden.nl/thumbs/w-green-02-b7034b42da5b7a20b03d5e6d87289f79.jpg 1600w,
		                http://www.bolden.nl/thumbs/w-green-02-77d2f8cd64024ef48ea3d6eb2911e952.jpg 1800w" alt="W.GREEN">
		            <img class="showcase__image" sizes="100vw" srcset="http://www.bolden.nl/thumbs/w-green-03-09a46448493cbbee43597e1047040aad.jpg 400w,
		                http://www.bolden.nl/thumbs/w-green-03-6fb6fcd8f396b1238fa329d3363178e2.jpg 600w,
		                http://www.bolden.nl/thumbs/w-green-03-3239aa14b45cee8d67cc4c427c9a4728.jpg 800w,
		                http://www.bolden.nl/thumbs/w-green-03-115178e7eab0aef73cdcc5035672b2e7.jpg 1000w,
		                http://www.bolden.nl/thumbs/w-green-03-93c06ae52c53a4367021b88a8805c61f.jpg 1400w,
		                http://www.bolden.nl/thumbs/w-green-03-7d440b035595503bdb2389c5cfe3d8d8.jpg 1600w,
		                http://www.bolden.nl/thumbs/w-green-03-089894589b1aefe11aaed5af90ba8a34.jpg 1800w" alt="W.GREEN">
		            <img class="showcase__image" sizes="100vw" srcset="http://www.bolden.nl/thumbs/w-green-05-3c52ccd54607cb11b46d14ff57df4d2e.jpg 400w,
		                http://www.bolden.nl/thumbs/w-green-05-d51a3de873058936abd7d91152d46425.jpg 600w,
		                http://www.bolden.nl/thumbs/w-green-05-505223b4b73d6d9ab827382108f5f9c2.jpg 800w,
		                http://www.bolden.nl/thumbs/w-green-05-8f7e93ff377b435eb1c074cf631412a1.jpg 1000w,
		                http://www.bolden.nl/thumbs/w-green-05-160ec8900cc4f024c6ec542de3dd63f1.jpg 1400w,
		                http://www.bolden.nl/thumbs/w-green-05-5f41600d07faf73e503b3889466e8a9c.jpg 1600w,
		                http://www.bolden.nl/thumbs/w-green-05-a5bc8a93efb02b345e849c4ffb2f80e2.jpg 1800w" alt="W.GREEN">
		            <img class="showcase__image" sizes="100vw" srcset="http://www.bolden.nl/thumbs/w-green-04-a3dc9d9e4709116680db828b0977c2e8.jpg 400w,
		                http://www.bolden.nl/thumbs/w-green-04-b25e08b97b50162df436a3a249d70730.jpg 600w,
		                http://www.bolden.nl/thumbs/w-green-04-17f07b884813959b9369e89ff7d021c7.jpg 800w,
		                http://www.bolden.nl/thumbs/w-green-04-a54812008f5beac6bcb5f3397fe13be1.jpg 1000w,
		                http://www.bolden.nl/thumbs/w-green-04-1f40c5e0b0498e04ceb877c1b3a7a335.jpg 1400w,
		                http://www.bolden.nl/thumbs/w-green-04-459c5115c036ede410f16f160bc840a1.jpg 1600w,
		                http://www.bolden.nl/thumbs/w-green-04-03ca10ddc56cdca32b515652c7b0d760.jpg 1800w" alt="W.GREEN">
			 	</div>
			</div> <!-- .portfolio-content -->\
			
			
		</div>
	</div> */ ?>
<?php require_once('inc/footer.php'); ?>
	