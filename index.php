<?php require_once('inc/header.php'); ?>
	<!-- ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
	HERO
	oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo -->
	<div id="homepage" class="batas_bg clearfix clear">
		<section id="started" class="full_height antiauto" style="background: #e9bb05">
			<div class="mainSlideshow">
				<!-- SLIDE LOOP -->
				<div class="gallery-cell">
					<div class="v-align">
						<!-- The Text --> 
						<div class="ms-caption">
							<div class="weare">
								We are
							</div>
							<div class="hello">
								<h2 data-heading="A WEB AGENCY">A WEB AGENCY</h2>
							</div>
						</div>

						<!-- Mirror Images -->
						<div class="mirrorwrapper">
							<div class="mirrorimage">
								<div class="inner">
									<div class="inner_2">
										<img class="layer" data-depth-x="0.60" data-depth="0.40" src="assets/img/slides/slide-1.jpg" alt=""">
									</div>
								</div>
							</div>
							<div class="mirrorimage">
								<div class="inner">
									<div class="inner_2_invert">
										<img class="layer" data-depth-x="-0.60" data-depth="0.40" src="assets/img/slides/slide-1-flipped.jpg" alt="">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- SLIDE LOOP -->
				<div class="gallery-cell">
					<div class="v-align">
						<!-- The Text --> 
						<div class="ms-caption">
							<div class="weare">
								We often meet brands to make
							</div>
							<div class="hello">
								<h2 data-heading="RESPONSIVE WEBSITE">RESPONSIVE WEBSITE</h2>
							</div>
						</div>

						<!-- Mirror Images -->
						<div class="mirrorwrapper">
							<div class="mirrorimage">
								<div class="inner">
									<div class="inner_2">
										<img class="layer" data-depth-x="0.60" data-depth="0.40" src="assets/img/slides/slide-2.jpg" alt=""">
									</div>
								</div>
							</div>
							<div class="mirrorimage">
								<div class="inner">
									<div class="inner_2_invert">
										<img class="layer" data-depth-x="-0.60" data-depth="0.40" src="assets/img/slides/slide-2-flipped.jpg" alt="">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- SLIDE LOOP -->
				<div class="gallery-cell">
					<div class="v-align">
						<!-- The Text --> 
						<div class="ms-caption">
							<div class="weare">
								And we do have our own called
							</div>
							<div class="hello">
								<h2 data-heading="PLAYWORLD.ID">PLAYWORLD.ID</h2>
							</div>
						</div>

						<!-- Mirror Images -->
						<div class="mirrorwrapper">
							<div class="mirrorimage">
								<div class="inner">
									<div class="inner_2">
										<img class="layer" data-depth-x="0.60" data-depth="0.40" src="assets/img/slides/slide-3.jpg" alt=""">
									</div>
								</div>
							</div>
							<div class="mirrorimage">
								<div class="inner">
									<div class="inner_2_invert">
										<img class="layer" data-depth-x="-0.60" data-depth="0.40" src="assets/img/slides/slide-3-flipped.jpg" alt="">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- SLIDE LOOP -->
				<div class="gallery-cell">
					<div class="v-align">
						<!-- The Text --> 
						<div class="ms-caption">
							<div class="weare">
								We're only few but we certainly
							</div>
							<div class="hello">
								<h2 data-heading="PROFESSIONAL">PROFESSIONAL</h2>
							</div>
						</div>

						<!-- Mirror Images -->
						<div class="mirrorwrapper">
							<div class="mirrorimage">
								<div class="inner">
									<div class="inner_2">
										<img class="layer" data-depth-x="0.60" data-depth="0.40" src="assets/img/slides/slide-4.jpg" alt=""">
									</div>
								</div>
							</div>
							<div class="mirrorimage">
								<div class="inner">
									<div class="inner_2_invert">
										<img class="layer" data-depth-x="-0.60" data-depth="0.40" src="assets/img/slides/slide-4-flipped.jpg" alt="">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- SLIDE LOOP -->
				<div class="gallery-cell">
					<div class="v-align">
						<!-- The Text --> 
						<div class="ms-caption">
							<div class="weare">
								And so we called ourself as
							</div>
							<div class="hello">
								<h2 data-heading="JAYADATA">JAYADATA</h2>
							</div>
						</div>

						<!-- Mirror Images -->
						<div class="mirrorwrapper">
							<div class="mirrorimage">
								<div class="inner">
									<div class="inner_2">
										<img class="layer" data-depth-x="0.60" data-depth="0.40" src="assets/img/slides/slide-5.jpg" alt=""">
									</div>
								</div>
							</div>
							<div class="mirrorimage">
								<div class="inner">
									<div class="inner_2_invert">
										<img class="layer" data-depth-x="-0.60" data-depth="0.40" src="assets/img/slides/slide-5-flipped.jpg" alt="">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</section>

		<section class="full_height" id="jdiservices" style="background-image: url(assets/img/consultant.jpg);">
			<div class="bigbg overlay-1"></div>
			<div class="bigbg overlay-2"></div>
			<div class="bigbg overlay-3"></div>
			<div class="bigbg overlay-4"></div>
			<div class="dottedpattern"></div>

			<!-- <div class="row">
				<div class="large-12 small-12 medium-12 column">
					<h2 class="whatwedotitle">WHAT WE DO</h2>
				</div>
			</div> -->

			<div class="v-align">
				<div class="row">
					<div class="large-6 small-12 column">
						<div class="service_box first" data-overlay="1">
							<div class="numeric">
								1
							</div>
							<h3>Strategic Consultation</h3>
							<p>Website will represent your brand in digital market and we will make sure you receive the best advice how to make it good.</p>
						</div>
					</div>
					<div class="large-6 small-12 column">
						<div class="service_box" data-overlay="2">
							<div class="numeric">
								2
							</div>
							<h3>Responsive Website Development</h3>
							<p>Our designers will carve your brand identity into an ideal user interface with a good user experience in any device</p>
						</div>
					</div>
					<div class="clear clearfix"></div>
					<div class="large-6 small-12 column">
						<div class="service_box" data-overlay="3">
							<div class="numeric">
								3
							</div>
							<h3>Front and Back-end Development</h3>
							<p>
								We will make sure your website will have a good feature and be able to handle request in background for some daily task you need - like posting new picture or video
							</p>
						</div>
					</div>
					<div class="large-6 small-12 column">
						<div class="service_box" data-overlay="4">
							<div class="numeric">
								4
							</div>
							<h3>SMS Payment Gateway</h3>
							<p>
								we provide short messaging service to integrate with your website need such as OTP (One Time Password), SMS Voting and other form of one or two-way communication.
							</p>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="full_height" id="startup" style="background: #000;/*#069*/">
			<div class="startupcolumn">
				<div class="normal closed su_item first">
					<div class="mockup">
						<div class="imac">							
							<div class="inner-imac">							
								<img src="assets/img/portfolio/single/playworld-nobg.jpg" alt="">
							</div>
						</div>
					</div>
					<div class="su_text">
						<h3>Playworld in a words</h3>
						<p>
							Is a "social news and entertainment website" for Indonesian viewer. Our focus is on delivering a shareable content both text, image and video. 
						</p>
						<a href="http://playworld.id" class="btn btn-4 btn-4a" target="_blank">Check it Out<span>PLAYWORLD.ID</span></a>
					</div>
				</div>
				<div class="normal closed su_item second">
					<div class="mockup">
						<div class="imac">							
							<div class="inner-imac">							
								<img src="assets/img/portfolio/single/playworld-coins.jpg" alt="">
							</div>
						</div>
					</div>
					<div class="su_text">
						<h3>Engagement activity</h3>
						<p>
							To increase and retain the visitor we put quizzes and other online activity for people to race and collect the coins that can be redeemed into voucher or goods.
						</p>
						<a href="http://playworld.id" class="btn btn-4 btn-4a" target="_blank">Are you brands?<span>Collaborate with us</span></a>
					</div>
				</div>
				<div class="normal closed su_item third">
					<div class="mockup">
						<div class="imac">							
							<div class="inner-imac">							
								<img src="assets/img/portfolio/single/playworld-contribute.png" alt="">
							</div>
						</div>
					</div>
					<div class="su_text">
						<h3>Contribute and get earnings</h3>
						<p>
							Encourage people especially youngsters to contribute in playworld by submitting original content and get earning for every approved content
						</p>
						<a href="http://playworld.id" class="btn btn-4 btn-4a" target="_blank" style="text-transform: uppercase;">Submit your content<span>geat earnings right away</span></a>
					</div>
				</div>
			</div>
		</section>

		<section class="full_height" id="smspayment" style="position: relative; background: #5a3b53">
			<div class="sms_slideshow">
				<div class="carousel-cell">

					<div class="svg">
						<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 960 560">
						<!-- <path stroke-linecap="round" id="XMLID_24_" class="st0" d="M4045,131c-47,0-85,38-85,85v216c0,40.3-32.7,73-73,73v0h-142v0c-40.3,0-73-32.7-73-73V143.9
						c0-40.3-32.6-72.9-72.9-72.9h3.9h-219h1c-39.8,0-72,32.2-72,72v217.3c0,39.6-31.3,71.7-70,71.7h-146v0.3c-39.8,0-72-32.2-72-72V216
						v1.8c0-40.9-33.1-73.9-73.9-73.9c0,0-215,0-215,0c-39.8,0-72.1,32.2-72.1,72.1v216.7V432c0,39.8-32.2,72-72,72h-216
						c-39.6,0-72-31.7-72-71.3V288c0-39.8-32.2-72-72-72v0h-143.7c-39.4,0-71.3-33.6-71.3-73c0-39.6-32.1-71.7-71.7-71.7h0.7h-146
						c-40.3,0-73,31.3-73,71.7v217.3c0,39.8-32.2,72-72,72h1h-75h2.7c-39.8,0-72-32.2-72-72V215c0-39.8-32.2-72-72-72h-145.3h1.3
						c-39.8,0-72,32.2-72,72v145.3c0,39.8-32.2,72-72,72H1081c-36.5,0-66-32.2-66-72v-66.4c0-24.3-19.7-44-44-44v0H798.5
						c-16.3,0-29.5-13.2-29.5-29.5V62.2c0-7.1-6-12.8-13.5-12.8h-39.2c-9.5,0-17.1,5.7-17.1,12.8v111.6c0,7.3-5.9,13.2-13.2,13.2h-81.8
						c-13.1,0-23.7,8.2-23.7,18.2v110c0,10.4-10.4,19.8-20.8,19.8h-85.4c-9.6,0-17.3-7.7-17.3-17.3V128.9c0-15.3-10.1-27.7-22.7-27.7
						h-86.2c-15.7,0-28.5,12.2-28.5,27.3v116.9c0,11.3-11.7,20.4-26.2,20.4H177.4c-20.2,0-36.5-14.3-36.5-31.9V125.3
						c0-29.7-21.9-53.7-49.1-53.7H2"></path> -->
							<path stroke-linecap="round" id="XMLID_24_" class="st0" d="M0,158.6c126.4,0,126.4,242.7,252.8,242.7c126.4,0,126.4-242.7,252.8-242.7
		c126.4,0,126.4,242.7,252.8,242.7c126.4,0,126.4-242.7,252.8-242.7c126.4,0,126.4,242.7,252.8,242.7
		c126.4,0,126.4-242.7,252.8-242.7c126.4,0,126.4,242.7,252.8,242.7c126.4,0,126.4-242.7,252.8-242.7s126.4,242.7,252.8,242.7
		s126.4-242.7,252.8-242.7s126.4,242.7,252.8,242.7c126.4,0,126.4-242.7,252.8-242.7c126.4,0,126.4,242.7,252.8,242.7
		c126.4,0,126.4-242.7,252.8-242.7c126.4,0,126.4,242.7,252.8,242.7c126.4,0,126.4-242.7,252.8-242.7"></path>
						</svg>
					</div>
					<div class="v-align">
						<div class="row" data-equalizer>
							<div class="large-6 column" data-equalizer-watch>
								<div class="v-align-2">
									<div class="v-align-2-inner text-center" style="width: 100%;">
										<div class="iconic-wrapper">
											<img src="assets/img/13.png" alt="" class="iconical">
											<div class="hexagon"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="large-6 column" data-equalizer-watch>
								<div class="v-align-2">
									<div class="v-align-2-inner">
										<p>By integrating our one or two-way communication using short message service it's been allow you to create another engagement method to interact with your customer</p>
										<a href="#" class="btn btn-4 btn-4a" id="seesample">See the Sample</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="carousel-cell">
					<div class="v-align">
						<div class="row" data-equalizer>
							<div class="large-6 column" data-equalizer-watch>
								<div class="v-align-2">
									<div class="v-align-2-inner" style="width: 100%;">
										<div class="iconic-wrapper">
											<img src="assets/img/199.png" alt="" class="iconical" style="max-width: 600px">
											<div class="hexagon"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="large-6 column" data-equalizer-watch>
								<div class="v-align-2">
									<div class="v-align-2-inner">
										<p>Secure your customer credential by adding OTP or One Time Password when log-in or when making a payment</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="carousel-cell">
					<div class="v-align">
						<div class="row" data-equalizer>
							<div class="large-6 column" data-equalizer-watch>
								<div class="v-align-2">
									<div class="v-align-2-inner" style="width: 100%;">
										<div class="iconic-wrapper">
											<img src="assets/img/100.png" alt="" class="iconical" style="max-width: 600px">
											<div class="hexagon"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="large-6 column" data-equalizer-watch>
								<div class="v-align-2">
									<div class="v-align-2-inner">
										<p style="font-size: 30px;">Alert your customer with SMS Push Notification</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="carousel-cell">
					<div class="v-align">
						<div class="row" data-equalizer>
							<div class="large-6 column" data-equalizer-watch>
								<div class="v-align-2">
									<div class="v-align-2-inner text-center" style="width: 100%;">
										<div class="iconic-wrapper">
											<img src="assets/img/66.png" alt="" class="iconical">
										</div>
									</div>
								</div>
							</div>
							<div class="large-6 column" data-equalizer-watch>
								<div class="v-align-2">
									<div class="v-align-2-inner">
										<p style="font-size: 30px;"><strong>GOT ANOTHER IDEAS?</strong></p>
										<a href="contact.php" class="btn btn-4 btn-4a">Talk to us now</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>

			<div class="sms_slideshow_button">
				<button class="button button--previous"><i class="fa fa-chevron-left"></i></button>
				<div class="button-group button-group--cells">
					<div class="button is-selected tip" data-tip="Introduction">
						<div id="prgrs_1" class="third circle"></div>
					</div>
					<div class="button tip" data-tip="OTP">
						<div id="prgrs_2" class="third circle"></div>
					</div>
					<div class="button tip" data-tip="SMS Alert">
						<div id="prgrs_3" class="third circle"></div>
					</div>
					<div class="button tip" data-tip="Your Idea?">
						<div id="prgrs_4" class="third circle"></div>
					</div>
				</div>
				<button class="button button--next"><i class="fa fa-chevron-right"></i></button>
			</div>
		</section>
	</div>
	

	<!-- ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
	BRANDS AND PARTNER
	oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo -->
	<section id="clients" class="full_height">
		<div class="v-align">
			<div class="row">
				<div class="large-12 colunn">
					<div class="relative_2">
						<div class="clientSlideContainer">
							<ul class="clientList active">
								<li>
									<div>
										<img src="assets/img/clients/0ssbn-default.png" alt="">
									</div>
								</li>
								<li>
									<div>
										<img src="assets/img/clients/pizzahut-logo-small-min.png" alt="">
									</div>
								</li>
								<li>
									<div>
										<img src="assets/img/clients/kawasaki-logo-small-min.png" alt="">
									</div>
								</li>
								<li>
									<div>
										<img src="assets/img/clients/ceres-logo-small-min.png" alt="">
									</div>
								</li>
								<li>
									<div>
										<img src="assets/img/clients/0sssbn-sinarmas.png" alt="">
									</div>
								</li>
								<li>
									<div>
										<img src="assets/img/clients/bca-logo-small-min.png" alt="">
									</div>
								</li>
								<li>
									<div>
										<img src="assets/img/clients/garnier-logo-small-min.png" alt="">
									</div>
								</li>
								<li>
									<div>
										<img src="assets/img/clients/lufthansa-logo2-small-min.png" alt="">
									</div>
								</li>
							</ul>
							<ul class="clientList">
								<li>
									<div>
										<img src="assets/img/clients/natasha-logo-small-min.png" alt="">
									</div>
								</li>
								<li>
									<div>
										<img src="assets/img/clients/mataharimall-logo-small-min.png" alt="">
									</div>
								</li>
								<li>
									<div>
										<img src="assets/img/clients/olivespain-logo-small-min.png" alt="">
									</div>
								</li>
								<li>
									<div>
										<img src="assets/img/clients/skkmigas-logo-small-min.png" alt="">
									</div>
								</li>
								<li>
									<div>
										<img src="assets/img/clients/partners/adk-min.png" alt="">
									</div>
								</li><li>
									<div>
										<img src="assets/img/clients/partners/guerilla-logo-min.png" alt="">
									</div>
								</li>
								<li>
									<div>
										<img src="assets/img/clients/partners/inspire.png" alt="">
									</div>
								</li>
								<li>
									<div>
										<img src="assets/img/clients/partners/PUB_Logo_Groupe_RVB.png" alt="">
									</div>
								</li>
							</ul>
							<ul class="clientList">
								<li>
									<div>
										<img src="assets/img/clients/partners/Valuklik-Logo-Original-on-Transparent1.png" alt="">
									</div>
								</li>
								<li>
									<div>
										<img src="assets/img/clients/partners/saatchi-logo-stacked-transparent@2x.png" alt="">
									</div>
								</li>
								<li><div></div></li>
								<li><div></div></li>
								<li><div></div></li>
								<li><div></div></li>
								<li><div></div></li>
								<li><div></div></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>


		<div class="clientPages">
			<div data-index="1" class="active">1</div>
			<div data-index="2">2</div>
			<div data-index="3">3</div>
		</div>
			
	</section>

	<!-- ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
	PORTFOLIO
	oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo -->
	<section id="works" class='full_height portfolio_section noline'>
		<div class="v-align">
			<br><br><br><br>
			<div class="row">
				<div class="case-row">
					<div class="large-4 medium-6 small-12 column">
						<div class="case-item">
							<a href="case/golkar.php">
								<img src="assets/img/portfolio/simple/golkar.jpg" alt="">
							</a>
						</div>
					</div>
					<div class="large-4 medium-6 small-12 column">
						<div class="case-item">
							<a href="case/silverqueen-maldives.php">
								<img src="assets/img/portfolio/simple/maldives.jpg" alt="">
							</a>
						</div>
					</div>
					<div class="large-4 medium-6 small-12 column">
						<div class="case-item">
							<a href="case/garnier-project-1-week.php">
								<img src="assets/img/portfolio/simple/garnier.jpg" alt="">
							</a>
						</div>
					</div>
					<div class="large-4 medium-6 small-12 column">
						<div class="case-item">
							<a href="case/mataharimall.php">
								<img src="assets/img/portfolio/simple/matahari.png" alt="">
							</a>
						</div>
					</div>
					<div class="large-4 medium-6 small-12 column">
						<div class="case-item">
							<a href="case/chunkybar.php">
								<img src="assets/img/portfolio/simple/silverqueen.jpg" alt="">
							</a>
						</div>
					</div>
					<div class="large-4 medium-6 small-12 column">
						<div class="case-item">
							<a href="case/silverqueen-valentine.php">
								<img src="assets/img/portfolio/simple/sq-valentine.jpg" alt="">
							</a>
						</div>
					</div>
				</div>
				<div class="large-12 column text-right">
					<a href="cases.php" class="btn btn-4 btn-4a darkbtn" style="margin-top: 30px;">&nbsp;&nbsp;&nbsp;&nbsp;SEE MORE&nbsp;&nbsp;&nbsp;&nbsp;</a>
				</div>
			</div>
		</div>
	</section>

	<section id="career" class="full_height">
		<div class="v-align">
			<div class="row hiring text-dark">
				<div class="large-8 column">
					<h2>WE ARE HIRING</h2>

					<p>Competitive remuneration is the one of our key strength why you should join and be part of Jayadata but as long you have a competitive resume to offer. Our good vibe and less noise surroundings will raise your mood to work. In Jayadata you are so visible, be heard and be part in each important decision.</p>
					<a href="http://www.jobstreet.co.id/id/job/2092606/sources/2/origin/id" class="btn btn-4 btn-4a darkbtn">Click here for WEB DESIGNER</a>
					<div class="clear"></div>

					<a href="#" class="btn btn-4 btn-4a darkbtn">Click here for PHP PROGRAMMER</a>
					<div class="clear"></div>
					
					<a href="#" class="btn btn-4 btn-4a darkbtn">Click here for CONTENT WRITER</a>
					<div class="clear"></div>
				</div>
				</div>
			</div>			
		</div>
	</section>

	<section id="aboutjdi" class="full_height clear clearfix">
		<div class="row">
			<div class="large-7 small-12 column" style="color: #fff">
				<div class="contact-container">
					<div class="row" style="position: relative;">
						<div class="large-7 medium-9 small-12 column">
							<h3>THE OFFICE</h3>
							<p class="alamat">
								Kokan Permata Blok E29<br>
								Jl. Boulevard Bukit Gading Raya <br>
								Kelapa Gading, Jakarta Utara 14240<br>
								DKI Jakarta, Indonesia
							</p>
							<ul class="social">
								<li><a href="https://www.facebook.com/playworldID" target="_BLANK" class="facebook"><i class="fa fa-facebook"></i></a></li>
								<li><a href="https://twitter.com/PlayworldID" target="_BLANK" class="twitter"><i class="fa fa-twitter"></i></a></li>
								<li><a href="https://www.linkedin.com/company/3572988?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A3572988%2Cidx%3A1-1-1%2CtarId%3A1484290768224%2Ctas%3Ajayadata" target="_BLANK" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="https://www.instagram.com/playworldid/" target="_BLANK" class="instagram"><i class="fa fa-instagram"></i></a></li>
							</ul>
						</div>
						<a href="https://goo.gl/maps/pmBwNoTAro92" class="map3d" target="_blank"><img src="assets/img/jakarta.png" alt="" class="layer" data-depth-x="0.60" data-depth="0.40" ></a>
					</div>
				</div>
			</div>
			<div class="large-5 small-12 column">
				<div class="shortstory">
					<div class="shortstory_inner">
						<h2>About Jayadata</h2>
						<p>
							Jayadata or simply called as JDI has been operated since early 2013 from Kelapa Gading, North Jakarta. Our team only few but we put the right man on the right place  and so we can deliver a good quality product and services to all of our beloved clients.
							<br><a href="#" class="btn btn-4 btn-4a darkbtn">MORE ABOUT US</a>
						</p>
						
					</div>
				</div>
			</div>
		</div>

		<div class="footer_hack footer-hack">
			<?php require_once('inc/footer.php'); ?>
		</div>
	</section>
	<div></div>