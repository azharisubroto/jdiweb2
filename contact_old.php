<?php require_once('inc/header.php'); ?>
	<!-- ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
	HERO
	oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo -->
	<section id="contactpage hero" class="full_height" style="background: rgb(37, 154, 255) url(assets/img/map.png) no-repeat center center; background-size: 90%">
		<div class="donut"></div>
		<div class="rectangle"></div>
		<div class="v-align">
			<div class="row" data-equalizer>
				<div class="large-4 small-12 medium-12 column" data-equalizer-watch>
					<h2 class="slab pagetitle">CONTACT US</h2>
					<ul class="office-detail">
						<li>
							<h4>THE OFFICE</h4>
							<p>
								Kokan Permata Blok E29<br>
								Jl. Boulevard Bukit Gading Raya <br>
								Kelapa Gading, Jakarta Utara 14240<br>
								DKI Jakarta, Indonesia
							</p>
						</li>
						<li>
							<h4>CALL US</h4>
							<p>+62 21 293 85 381</p>
						</li>
						<li>
							<h4>Leave us an email</h4>
							<p>corporate@jayadata.co.id</p>
						</li>
					</ul>
				</div>
				<div class="large-8 small-12 medium-12 column" data-equalizer-watch>
					<div class="message-box dark-text">
						<div class="row" data-equalizer>
							<div class="large-8 small-12 medium-12 column" data-equalizer-watch>
								<h3>SEND US AN EMAIL</h3>
								<form action="#">
									<input type="text" name="nama" placeholder="Name">
									<input type="email" name="email" placeholder="Email">
									<input type="text" name="subject" placeholder="Subject">
									<textarea name="message" id="" cols="30" rows="7" placeholder="Message"></textarea>
									<button>SEND</button>
								</form>
							</div>
							<div class="large-4 small-12 medium-12 column" data-equalizer-watch>
								<iframe class="jdimap" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2804.939927443119!2d106.8924428897769!3d-6.159665285918823!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f53c2376570f%3A0x9dba0f4c51294090!2sPT.+Jayadata+Indonesia!5e0!3m2!1sen!2sid!4v1483008001262" frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php require_once('inc/footer.php'); ?>