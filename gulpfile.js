// Define requires
var gulp        = require('gulp'),
    gutil       = require('gulp-util'),
    sass        = require('gulp-sass'),
    rename      = require('gulp-rename'),
    notify      = require('gulp-notify'),
    sourcemaps  = require('gulp-sourcemaps'),

    browserify  = require('browserify'),
    watchify    = require('watchify'),
    source      = require('vinyl-source-stream'),
    bufferivy   = require('vinyl-buffer'),

    concat      = require('gulp-concat'),
    uglify      = require('gulp-uglify'),
    imagemin    = require('gulp-imagemin'),
    pngquant    = require('imagemin-pngquant'),
    jpgrecomp   = require('imagemin-jpeg-recompress'),
    del         = require('del'),

    browserSync = require('browser-sync'),
    reload      = browserSync.reload;

// Functions
function handleErrors() {
    var args = Array.prototype.slice.call(arguments);
    notify.onError({
        title: "Compile Error",
        message: "<%= error.message %>"
    }).apply(this, args);
    this.emit('end');
}

// BrowserSync
gulp.task('browser-sync', function() {
    browserSync({
        proxy: {
            target: "http://azhari/JDIweb_2",
        }
    });
});

// Sass Compiler
gulp.task('sass-dev', function () {
    return gulp.src('source/scss/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'expanded'
            }).on('error', sass.logError))
        .pipe(rename('app-dist.css'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('assets/css/'))
        .pipe(reload({
            stream: true
            }));
});

gulp.task('sass-build', function () {
    return gulp.src('source/scss/**/*.scss')
        .pipe(sass({
            outputStyle: 'compressed'
            }).on('error', sass.logError))
        .pipe(rename('app-dist.css'))
        .pipe(gulp.dest('assets/css/'));
});

// Browserify bundle
var sourceFile  = './source/js/app.js',
    destFolder  = './assets/js/',
    destFile    = 'app-dist.js';

function buildScript(file, watch) {
    var propsDev = {
        entries: sourceFile,
        debug: true,
        cache: {},
        packageCache: {}
    };

    var propsBuild = {
        entries: sourceFile,
        debug: false,
        cache: {},
        packageCache: {}
    };

    var bundler = watch ? watchify(browserify(propsDev)) : browserify(propsBuild);

    function rebundle() {
        var bundling = bundler.bundle();
        return bundling.on('error', handleErrors)
            .pipe(source(destFile))
            .pipe(bufferivy())
            .pipe(sourcemaps.init({loadMaps: true}))
            .pipe(uglify({
                mangle: true,
                compress: true,
                preserveComments: false // 'license'
                })).on('error', gutil.log)
            .pipe(sourcemaps.write('./'))
            .pipe(gulp.dest(destFolder));
    }

    bundler.on('update', function() {
        rebundle();
        gutil.log('Rebundle...');
        reload();
    });
    return rebundle();

};

gulp.task('js-dev', function() {
    return buildScript(sourceFile, true);
});

gulp.task('js-build', function() {
    return buildScript(sourceFile, false);
});

// Conventional JS Concatenate
gulp.task('js', function() {
    return gulp.src('source/js/*.js')
        .pipe(sourcemaps.init())
        .pipe(concat('app-dist.js'))
        .pipe(gulp.dest('assets/js/'))
        .pipe(rename('app-dist.min.js'))
        .pipe(uglify({
            mangle: true,
            preserveComments: 'license'
            }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('assets/js/'));
});

// Build clean up
gulp.task('clean', function() {
    return del(['assets/css/app-dist.css.map', 'assets/js/app-dist.js.map']);
});

// Optimize images
gulp.task('img', function() {
    return gulp.src('source/img/**/*')
    .pipe(imagemin({
        plugins: [
            imagemin.jpegtran({
                progressive: true
                }),
            imagemin.optipng({
                optimizationLevel: 7
                }),
            imagemin.gifsicle({
                interlaced: true
                }),
            imagemin.svgo(),
            jpgrecomp({
                progressive: true,
                quality: 'medium',
                loops: 9,
                min: 50,
                max: 95
                })
            ],
        use: [
            pngquant({
                speed: 3,
                quality: 10,
                verbose: true,
                })
            ]
        })
    )
    .pipe(gulp.dest('assets/img'));
});


// Default task
gulp.task('default', ['sass-dev', 'js-dev', 'browser-sync'], function () {
    gulp.watch('source/scss/**/*.scss', ['sass-dev']);
    gulp.watch('*.html').on('change', reload);
    gulp.watch('**/*.php').on('change', reload);
});

// Deploy
gulp.task('build', ['sass-build', 'js-build', 'clean']);