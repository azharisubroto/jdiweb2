<?php require_once('inc/header.php'); ?>
	
	<!-- ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
	HERO
	oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo -->
	<section class="half_height hero">
		<div class="intro text-center">
			<h1 class="slab" data-heading="ABOUT">ABOUT</h1>

			<div class="hero_intro">
				<p>
					PT. Jayadata Indonesia has been operated since early 2013, since then we've been involved in various project with different brands and clients from FMCG to theGovernment. We're only focus on website development but along the way we've open for any chance that we think we capable to made it. 
				</p>

				<p>
					We simply called our team as JDI the abbreviation from JAYADATA. We don't have any poetic quote to use as a jargon, but we believe with our good teamwork and having a good jokes at the afternoon, JDI will grow and strong than ever.
				</p>

				<p>
					With humble, we introduce JDI Squad
				</p>
			</div>
		</div>
	</section>

	<!-- ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
	JDI : JOY TO BRING DIGITAL IDEA TO LIFE
	oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo -->
	<section class="std_section bringtop">

		<div class="row member-grid">
			<div class="member-grid-inner" data-equalizer data-equalize-by-row="true">
				<div class="large-4 small-6 medium-6 column">
					<div class="member-wrapper" data-equalizer-watch>
						<div class="member-photo">
							<img src="assets/img/team/faisal_a.jpg" alt="" class="cover">
							<img src="assets/img/team/faisal_b.jpg" alt="" class="revealer">
						</div>
						<div class="member-info">
							<div class="member-name">
								FAISAL MURKADI
							</div>
							<div class="member-position">
								Operational Director / COO
							</div>
							<ul class="member-social">
								<li class="linkedin"><a href="https://id.linkedin.com/in/faisalmurkadi"><i class="fa fa-linkedin"></i></a></li>
								<li class="twitter"><a href="//twitter.com/icalizers"><i class="fa fa-twitter"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="large-4 small-6 medium-6 column">
					<div class="member-wrapper" data-equalizer-watch>
						<div class="member-photo">
							<img src="assets/img/team/arief_a.jpg" alt="" class="cover">
							<img src="assets/img/team/arief_b.jpg" alt="" class="revealer">
						</div>
						<div class="member-info">
							<div class="member-name">
								ARIEF AKBAR
							</div>
							<div class="member-position">
								Finance Director / CFO
							</div>
							
							<ul class="member-social">
								<li class="linkedin"><a href="https://id.linkedin.com/in/arief-akbar-a245b189"><i class="fa fa-linkedin"></i></a></li>
								<li class="twitter"><a href="//twitter.com/arief_akbar"><i class="fa fa-twitter"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="large-4 small-6 medium-6 column">
					<div class="member-wrapper" data-equalizer-watch>
						<div class="member-photo">
							<img src="assets/img/team/suharti_a.jpg" alt="" class="cover">
							<img src="assets/img/team/suharti_b.jpg" alt="" class="revealer">
						</div>
						<div class="member-info">
							<div class="member-name">
								SUHARTI
							</div>
							<div class="member-position">
								HR &amp; General Affairs
							</div>
							
							<ul class="member-social">
				 				<li class="linkedin"><a href="https://id.linkedin.com/in/suharti-baihaqi-205654a6/en"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="large-4 small-6 medium-6 column">
					<div class="member-wrapper" data-equalizer-watch>
						<div class="member-photo">
							<img src="assets/img/team/fahli_a.jpg" alt="" class="cover">
							<img src="assets/img/team/fahli_b.jpg" alt="" class="revealer">
						</div>
						<div class="member-info">
							<div class="member-name">
								FAHLI RIZA
							</div>
							<div class="member-position">
								Content &amp; Social Media Planner
							</div>
							
							<ul class="member-social">
								<li class="linkedin"><a href="https://id.linkedin.com/in/fahli-riza-56293811b"><i class="fa fa-linkedin"></i></a></li>
								<li class="twitter"><a href="//twitter.com/fahlyriza_182"><i class="fa fa-twitter"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="large-4 small-6 medium-6 column">
					<div class="member-wrapper" data-equalizer-watch>
						<div class="member-photo">
							<img src="assets/img/team/eko_a.jpg" alt="" class="cover">
							<img src="assets/img/team/eko_b.jpg" alt="" class="revealer">
						</div>
						<div class="member-info">
							<div class="member-name">
								EKO SUGIARTO
							</div>
							<div class="member-position">
								Account Manager
							</div>
							
							<ul class="member-social">
								<li class="linkedin"><a href="https://id.linkedin.com/in/gideon-eko-sugiarto-putra-79080349"><i class="fa fa-linkedin"></i></a></li>
								<li class="twitter"><a href="//twitter.com/eko740"><i class="fa fa-twitter"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- <div class="large-4 small-6 medium-6 column">
					<div class="member-wrapper" data-equalizer-watch>
						<div class="member-photo">
							<img src="assets/img/team/juma_a.jpg" alt="" class="cover">
							<img src="assets/img/team/juma_b.jpg" alt="" class="revealer">
						</div>
						<div class="member-info">
							<div class="member-name">
								AHMAD JUMA
							</div>
							<div class="member-position">
								Sr. Back-end Programmer
							</div>
							
							<ul class="member-social">
								<li class="linkedin"><a href="https://id.linkedin.com/in/ahmad-juma-33571867/en"><i class="fa fa-linkedin"></i></a></li>
								<li class="twitter"><a href="//twitter.com/rijkaard_ahmad"><i class="fa fa-twitter"></i></a></li>
							</ul>
						</div>
					</div>
				</div> -->
				<!-- <div class="large-4 small-6 medium-6 column">
					<div class="member-wrapper" data-equalizer-watch>
						<div class="member-photo">
							<img src="assets/img/team/azhari_a.jpg" alt="" class="cover">
							<img src="assets/img/team/azhari_b.jpg" alt="" class="revealer">
						</div>
						<div class="member-info">
							<div class="member-name">
								AZHARI SUBROTO
							</div>
							<div class="member-position">
								Sr. Frontend Developer
							</div>
							
							<ul class="member-social">
								<li class="linkedin"><a href="https://id.linkedin.com/in/azhari-subroto-1891464a"><i class="fa fa-linkedin"></i></a></li>
								<li class="twitter"><a href="//twitter.com/subrotoazhari"><i class="fa fa-twitter"></i></a></li>
							</ul>
						</div>
					</div>
				</div> -->
				<div class="large-4 small-6 medium-6 column">
					<div class="member-wrapper" data-equalizer-watch>
						<div class="member-photo">
							<img src="assets/img/team/melynda_a.jpg" alt="" class="cover">
							<img src="assets/img/team/melynda_b.jpg" alt="" class="revealer">
						</div>
						<div class="member-info">
							<div class="member-name">
								MELYNDA NUR AZIZIYAH
							</div>
							<div class="member-position">
								Customer Service
							</div>
							
							<!-- <ul class="member-social">
								<li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
								<li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
							</ul> -->
						</div>
					</div>
				</div>
				<!-- <div class="large-4 small-6 medium-6 column">
					<div class="member-wrapper" data-equalizer-watch>
						<div class="member-photo">
							<img src="assets/img/team/wes_a.jpg" alt="" class="cover">
							<img src="assets/img/team/wes_b.jpg" alt="" class="revealer">
						</div>
						<div class="member-info">
							<div class="member-name">
								WEST ASEP SUBEKTI
							</div>
							<div class="member-position">
								Finance Staff
							</div>
							
							<ul class="member-social">
								<li class="linkedin"><a href="https://www.linkedin.com/in/west-asep-subekti-33b122103/"><i class="fa fa-linkedin"></i></a></li>
								<li class="twitter"><a href="https://twitter.com/westasepsubekti"><i class="fa fa-twitter"></i></a></li>
							</ul>
						</div>
					</div>
				</div> -->
			</div>
		</div>
	</section>
	<section class="clear clearfix small_section" style="height: auto!important;min-height:inherit!important;padding:0;background: transparent;">
		<div>
			<img src="assets/img/team/allteam.jpg" alt="">
		</div>
	</section>
<?php require_once('inc/footer.php'); ?>
	